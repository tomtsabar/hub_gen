% =============================================================================
\section{Terms and Defintions}
\label{sec:terms}
% =============================================================================
In this section we describe the structure of our fixtures and their properties.

% -----------------------------------------------------------------------------
\subsection{Fixture Structure}
\label{ssec:terms:structure}
% -----------------------------------------------------------------------------
\begin{definition}[$\alpha$-extrusion of a polygon and base polygon of an
  $\alpha$-extrusion] Let $L$ denote a polygon in 3D space; let $v$
  denote the normal to the plane containing $L$, and let $v_{\alpha}$
  denote the scaled normal of length $\alpha$. The
  \emph{$\alpha$-extrusion} of $L$ is a polyhedron $P$ in 3D space,
  which is the extrusion of $L$ along $v_{\alpha}$. The polygon $L$ is
  referred to as the \emph{base polygon} of $P$; see
  Figure~\ref{fig:alpha-extrusion}.
\end{definition}

\begin{figure}[!ht]
  \centering
  \subfloat[]{\label{fig:ae:f}
  \begin{tikzpicture}[node distance=5cm]

  \node (img) {\includegraphics[width=0.25\linewidth]{polygone_1_a}};
  \node [below left,text width=5cm,align=left,yshift=-4ex, xshift=17ex ] at (img.north east){\large $v$};
  \end{tikzpicture}}
\subfloat[]{\label{fig:ae:e}
\begin{tikzpicture}[node distance=5cm]
\node (img) {\includegraphics[width=0.25\linewidth]{polygone_extruded_1_a}};
\node [below left,text width=5cm,align=left,yshift=-11.2ex, xshift=13ex ] at (img.north east){\large $\alpha$};
\end{tikzpicture}}
  \caption[]{(\subref*{fig:ae:f}) A polygon $L$. (\subref*{fig:ae:e}) The
    $\alpha$-extrusion of $L$.}
  \label{fig:alpha-extrusion}
\end{figure}

For simplicity, we occasionally use the terms vertices, edges, and
facets to abbreviate the references to their respective geometric
embeddings (which are points, segments, and polygons, respectively).
In particular, we use the abbreviation $\alpha$-extrusion of a facet
$f$ of some polyhedron to refer to the $\alpha$-extrusion $P$ of the
geometric embedding of the facet $f$, and we refer to the facet of $P$
that overlaps with $f$ as the base facet of the $\alpha$-extrusion $P$.

Consider an input polyhedron $P$ that represents a workpiece. The
structure of a fixture of $P$ resembles the structure of a hand; it is
the union of a single polyhedral part referred to as the \emph{palm},
several polyhedral parts, referred to as \emph{fingers}, which are
extensions of the \emph{palm}, and semi-rigid joints that connect the
palm and the fingers. Each \emph{finger} consists of two polyhedral
parts, namely, \emph{body} and \emph{gripper} and the semi-rigid joint
between the \emph{body} and the \emph{gripper}. The various parts,
i.e., palm, bodies, grippers, and joints, are disjoint in their
interiors. (In configurations where the joints bent, some parts may
overlap with the joints.) In the following we describe these parts in
details.

%% \begin{definition}[Palm]
%% A \emph{palm} is an extrusion of a facet of $P$.
%% \end{definition}

%% \begin{definition}[Finger]
%% Fingers are extensions of the \emph{palm}. Each \emph{finger} consists
%% of two polyhedral parts, namely, \emph{body} and \emph{gripper} and
%% the semi-rigid joint between the \emph{body} and the \emph{gripper}.
%% \end{definition}

Let $G$ denote a holding fixture made of a palm, $k$ fingers,
$F_1,F_2,\ldots,F_k$, and corresponding joints. The palm is an
$\alpha_p$-extrusion of a facet $f_p$ of $P$. (The value of $\alpha_p$
is discussed below.) Consider a specific finger $F=F_i$ of $G$. The
body of $F$ is defined by one of the neighbouring facets of $f_p$,
denoted $f_b$. The gripper of $F$ is defined by one of the
neighbouring facets of $f_b$, denoted $f_g$, $f_g\neq f_p$. Let
$e_{pb}$ denote the common edge of $f_p$ and $f_b$, and let $e_{bg}$
denote the common edge of $f_b$ and $f_g$. Let $q_b$ denote the
quadrilateral defined by the points, which are the geometric embedding
of the vertices incident to $e_{pb}$ and $e_{bg}$. Note that in some
degenerate cases $e_{pb}$ and $e_{bg}$ are incident to a common
vertex; in these cases $q_b$ is a triangle not a quadrilateral. The
body is an $\alpha_b$-extrusion of $q_b$. (The value of $\alpha_b$ is
also discussed below.) Let $v$ denote the cross product of the vector
that corresponds to $e_{bg}$ and the normal to the plane containing
$f_g$ of length $\alpha_g$ (discussed below). Let $q_g$ denote the
quadrilateral defined by the two points, which are the geometric
embedding of the vertices incident to $e_{bg}$ and their translations
by $v$. The gripper is a $\alpha_g$-extrusion of $q_g$. The axis of the
joint that connects the palm and the body of $F$ coincides with
$e_{pb}$ and axis of the joint that connects the body of $F$ with is
gripper coincides with $e_{bg}$.

\newlength{\intextsepSaved}\setlength\intextsepSaved{\intextsep}%
\setlength{\intextsep}{0pt}%
\begin{wrapfigure}[12]{R}{5cm}
  \begin{tikzpicture}[node distance=5cm]
    \node (img) {\includegraphics[width=1\linewidth]{fix_coll_1}};
    \node [](A) at (2.2, 0.6) {$f_{b_2}$};
    \node [](B) at (0.2, 0.0) {$f_{g_1}$};
    \node [](D) at (2.25, 2.2) {$f$};
  \end{tikzpicture}
\end{wrapfigure}
For a complete view of a workpiece and its holding fixture see
Figure~\ref{fig:wrap}. Observe that the palm and the bodies and
grippers that comprise the fingers of the fixture in the figure differ
from the formal definitions above. The differences stem from practical
reasons. In particular, the parts in the figure have smaller volumes,
which (i) reduces fabrication costs, and (ii) resolves collision
between distinct fingers. In some degenerate cases, such as the one
depicted to the right, distinct fingers could have collided. In the
figure, the base facet of the gripper of one finger, $f_{g_1}$,
coincides with $f$, a facet of the workpiece. Likewise, the base facet
of the body of the other finger, $f_{b_2}$, also coincides with
$f$. Avoiding collision is achieved by simultaneously shrinking the
base facets $f_{g_1}$ and $f_{b_2}$.  Now, the gripper grips only the
tip of $f$ and the body is streching only on a slim part of the
workpiece facet. As another example, consider the body of a finger
depicted in Figure~\ref{fig:wrap}\subref{fig:fix1:t}, which is the
$\alpha_b$-extrusion of the quadrilateral $q_b$ in the figure. It is
defined by two points that lie in the interior of $e_{pb}$ and two
points that lie in the interior of $e_{bg}$, as opposed to the formal
definition above, where the endpoints of $e_{pb}$ and $e_{bg}$ define
the corresponding formal quadrilateral. Also, in reality, parts, and
in particular joints, are not fabricated separately, and the entire
fixture is made of the same flexible material. Instead of rotating
about the the joint axes, the entire fingers bend. The differences,
though, have no effect on the correctness of the proofs and algorithms
(which adhere to the formal definitions) presented in the sequel.
These structural changes, the structures of the joints, and the
extrusion values, merely determine the degree of flexibility and
strength of the fixture; see Section~\ref{sssec:terms:strength} and
Section~\ref{sssec:terms:flexibility}.\setlength\intextsep{\intextsepSaved}%

 \begin{figure}[!ht]
   \centering\subfloat[]{\label{fig:fix1:o}
     \begin{tikzpicture}
       \node (img) {\includegraphics[trim={4cm 0 1cm 0},clip ,width=0.25\linewidth]{fix_overall_arrow_1}};
       \node [](D) at (-2.5, 1.2) {Input};
       \node [](E) at (0, 2.2) {Palm};
       \node [](F) at (1, 1.6) {Finger};
   \end{tikzpicture}}
   \subfloat[]{\label{fig:fix1:f}
     \begin{tikzpicture}[node distance=10cm]
       \node (img) {\includegraphics[trim={3cm 0 2cm 0},clip ,width=0.25\linewidth]{fix_finger_arrow_1}};
       \node [](A) at (2.3, -0.5) {$e_{pb}$};
       \node [](B) at (2.2, 1.2) {$e_{bg}$};
       \node [](C) at (0.3, 1.6) {$v$};
       \node [](D) at (0, 2) {Gripper base facet opposite normal};
       \node [](E) at (0.43, -0.3) {Gripper};
       \node [](F) at (0.8, -1.18) {Body};
   \end{tikzpicture}}
   \subfloat[]{\label{fig:fix1:t}
     \begin{tikzpicture}
       \node (img) {\includegraphics[trim={4cm 0 1cm 0},clip, width=0.25\linewidth]{fix_top_arrow_1}};
       \node [](B) at (0.8, 1.4) {$q_{q}$};
       \node [](A) at (1.1, 0.8) {$q_{b}$};
   \end{tikzpicture}}
   \caption[]{Three different views of an input polyhedron (blue) and a
     snapping fixture (orange).}
   \label{fig:wrap}
\end{figure}

% -----------------------------------------------------------------------------
\subsection{The Configuration Space}
\label{ssec:terms:configuration-space}
% -----------------------------------------------------------------------------
The workpiece and its holding fixture form an assembly.  Each joint in
the fixture connects two parts; it enables the rotation of one part
with respect to the other about an axis.  Each joint adds one degree
of freedom (DOF) to the configuration space of the assembly.  Thus,
the configuration space of the assembly (which is the configuration
space of the fixture, assuming the workpiece is stationary) has $6+2k$
DOFs, where $k$ indicates the number of fingers.

In our context, the workpiece and its holding fixture are considered
assembled, if they are \emph{infinitesimally inseparable}. When two
polyhedra are infinitesimally inseparable, any linear motion applied
to one of the polyhedra causes a collision in the polyhedra interiors.

\begin{definition}[Serving configuration]
  The workpiece and the fixture are in the \emph{serving
    configuration} if (i) they are separated (that is, they are not
  assembled), and (ii) there exists a vector $v$, such that when the
  fixture is translated by $v$, as a result of some force applied in
  the direction of $v$, exploiting the flexibility of the joints of
  the fixture, the workpiece and the fixture become assembled.
\end{definition}
%% \efi{The above may not deserve a definition block, but you can keep it
%%   for now. Also, consider introducing another configuration, where the
%%   fixture is already in place, but its grippers are bent. Then, use it
%%   below in the ``Flexibility'' subsubsection.}

When the workpiece and its holding fixture are separated, the fixture
can be transformed without colliding with the workpiece to reach the
serving configuration.

\begin{figure}[ht]
  \centering
  \subfloat[][]{\label{fig:fix2:P}%
    \includegraphics[width=0.14\linewidth]{polyhedron}}\hfil
  \subfloat[][]{\label{fig:fix2:G}%
    \includegraphics[width=0.16\linewidth]{polyhedron_fix}}\hfil
  \subfloat[][]{\label{fig:fix2:G-wrap-P}%
    \includegraphics[width=0.15\linewidth]{polyhedron_asm}}\hfil
  \caption[]{(\subref*{fig:fix2:P}) $P$ (\subref*{fig:fix2:G}) $G$
    (\subref*{fig:fix2:G-wrap-P}) $P$ and $G$ are assembled}
  \label{fig:fixture}
\end{figure}

% -----------------------------------------------------------------------------
\subsection{Fixture Properties}
\label{ssec:terms:properties}
% -----------------------------------------------------------------------------
Given a polyhedral representation $P$ of a workpiece, we list some
properties that a polyhedral representation $G$, of a holding fixture
of $P$, possesses.

% -----------------------------------------------------------------------------
\subsubsection{Joint Flexibility}
\label{sssec:terms:flexibility}
% -----------------------------------------------------------------------------
The flexibility of the \emph{fingers} is an important consideration in
the design.  In order to construct a fixture as described above, a
joint that connects the body of a finger to the palm, and the joint
that connects the gripper of a finger to its body must allow the
rotation of the respective parts about the respective axes when force
is applied.  The subtleties of this flexibility is discussed below.

\setlength{\intextsep}{0pt}%
\begin{wrapfigure}[10]{r}{3cm}
  \centering\begin{tikzpicture}
  \node (img) {\includegraphics[width=1\linewidth]{finger_angle_1}};
  \node [](D) at (1.2, 0.6) {$\theta$};
  \node [](E) at (1.2, 0.1) {$a$};
  \node [](F) at (1.35,-0.3) {$b$};
  \draw [dotted,orange,thick](0,1.4) circle (0.4cm);
  \end{tikzpicture}
\end{wrapfigure}
For simplicity, assume that the states where all fingers of a fixture
are about to snap happen simultaneously in a single configuration.
Consider this configuration, and in particular, consider one finger
and the joint that connects this finger with the palm, as depicted in
the figure to the right. This configuration occurs a split of a second
before the assembly reaches the assembled state when transformed,
starting at the serving configuration. Let $\theta$ denote the angle
between the finger and the workpiece on a cutting plane perpendicular
to the axis of the joint.  Note that in the assembled configuration
$\theta$ equals $0$ for all fingers (and for all cutting planes). Let
$\theta_{c}$ denote the joint threshold angle, that is, the maximum
bending angle the finger can tolerate without breaking. The threshold
angle of every joint depends on the material and thickness of the
joint. $\theta$ is an angle of an isosceles triangle with one edge
lying on the body base-facet and another edge lying on the gripper
base-facet. Let $a$ and $b$ denote the lengths of these edges,
respectively. Notice that $a$ monotonically depends on the cutting
plane as we slide the plane along the axis of the joint. If the
axis of our joint and the axis of the joint that connects the body and
the gripper of our finger are parallel, $a$ is constant. The finger
will break when $\theta > \theta_{c}$.  Applying the law of cosines,
we get $b = a \sqrt{2(1 - \cos \theta)}$, which implies a maximal
value $b \leq \min(a) \sqrt{2(1 - \cos \theta_c)}$. On the other hand, the
characteristics of the material of the finger determine the minimal
value of $b$ that guaranties a secured grasp of the workpiece by the
gripper. The construction of a fixture $G$ is feasible, only if
selecting a proper value $b$ for every finger of $G$ is possible. Our
planners do not take in account flexibility
considerations.\setlength\intextsep{\intextsepSaved}%

% -----------------------------------------------------------------------------
\subsubsection{Gripping Strength}
\label{sssec:terms:strength}
% -----------------------------------------------------------------------------
Another consideration in the fixture design is the gripping strength.
The gripping strength of a finger is based on the angle between the
palm and the body of the finger and on the angle between the body of
the finger and the gripper of the finger. The gripping strength is in
opposite relation with these angles; that is, the smaller each one of
these angles is the stronger the gripping is. Our planner does not
take in account strength considerations.

% -----------------------------------------------------------------------------
\subsubsection{Spreading Degree}
\label{sssec:intro:spreading-degree}
% -----------------------------------------------------------------------------
\setlength{\intextsep}{0pt}%
\begin{wrapfigure}[6]{R}{3cm}
  \centering\includegraphics[width=3cm]{sphere_like_2}
\end{wrapfigure}
In this paper we restrict ourselves to holding fixtures that have
fingers with \emph{spreading degree} two, which means that the body of
every finger is a single $\alpha$-extrusion of a facet of $P$. Loosly
speaking, every finger (the body and the gripper) stretches over two
facets of $P$. Naturally, fingers with a higher spreading-degree reach
further. The figure to the right depicts an icosahedron that does not
admit a valid fixture with spreading degree two. Naturally, lifting
this restriction will enable the generation of fixtures for a larger
range of workpieces.\setlength\intextsep{\intextsepSaved}%

% -----------------------------------------------------------------------------
\subsection{Fixture Planning}
\label{ssec:terms:planning}
% -----------------------------------------------------------------------------
The sole objective of our fixture algorithms is minimizing the
generated-fixture complexity, that is, obtaining a fixture with the
minimal number of fingers. Our planner is of the exhaustive type. As
explained in Section~\ref{sec:algorithms}, it examines many different
possible candidates of fingers, before it reaches a conclusion. As
aforementioned, the planner generates fixtures of spreading degree
two. Extending the planner to enable the generation of fixtures with
an increased spreading degree (without further modifications), will
directly increase the search space exponentially, and likely render
the procedure infeasible in many cases.
