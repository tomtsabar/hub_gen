% =============================================================================
\section{Term,  Definitions, and Properties}
\label{sec:terms-properties}
% =============================================================================
In this section we describe the structure of our snapping fixtures and
their properties.

% -----------------------------------------------------------------------------
\subsection{Fixture Structure}
\label{ssec:terms:structure}
% -----------------------------------------------------------------------------
\begin{definition}[$\bm{\alpha}$-extrusion of a polygon and base polygon of an
  $\alpha$-extrusion] Let $L$ denote a polygon in 3D space; let $v$
  denote the normal to the plane containing $L$, and let $v_{\alpha}$
  denote the normal scaled to length $\alpha$. The
  \emph{$\alpha$-extrusion} of $L$ is a polyhedron $P$ in 3D space,
  which is the extrusion of $L$ along $v_{\alpha}$. The polygon $L$ is
  referred to as the \emph{base polygon} of $P$; see
  Figure~\ref{fig:alpha-extrusion}.
\end{definition}

\begin{figure}[!ht]
  \centering%
  \subfloat[]{\label{fig:ae:f}
    \begin{tikzpicture}[node distance=5cm]
      \node (img) {\includegraphics[width=0.25\linewidth]{../figs/polygone_1_a.png}};
      \node [above,yshift=-3, xshift=5] at (img.north){\large $v$};
  \end{tikzpicture}}
  \subfloat[]{\label{fig:ae:e}
    \begin{tikzpicture}[node distance=5cm]
      \node (img) {\includegraphics[width=0.25\linewidth]{../figs/polygone_extruded_1_a.png}};
      \node [yshift=-7, xshift=-6] at (img.center) {\large $\alpha$};
  \end{tikzpicture}}
  \setlength{\abovecaptionskip}{5pt}
  \setlength{\belowcaptionskip}{-5pt}
  \caption[]{(\subref*{fig:ae:f}) A polygon $L$. (\subref*{fig:ae:e}) The
    $\alpha$-extrusion of $L$.}
  \label{fig:alpha-extrusion}
\end{figure}

For simplicity, we occasionally use the terms vertices, edges, and
facets to abbreviate the references to their respective geometric
embeddings (which are points, segments, and polygons, respectively).
In particular, we use the abbreviation $\alpha$-extrusion of a facet
$f$ of some polyhedron to refer to the $\alpha$-extrusion $P$ of the
geometric embedding of the facet $f$, and we refer to the facet of $P$
that overlaps with $f$ as the base facet of the $\alpha$-extrusion $P$.

Consider an input polyhedron $P$ that represents a workpiece. The
structure of a fixture of $P$ resembles the structure of a hand; it is
the union of a single polyhedral part referred to as the \emph{palm},
several polyhedral parts, referred to as \emph{fingers}, which are
extensions of the \emph{palm}, and semi-rigid joints that connect the
palm and the fingers. Each \emph{finger} consists of two polyhedral
parts, namely, \emph{body} and \emph{gripper}, and the semi-rigid joint
between the \emph{body} and the \emph{gripper}. The various parts,
i.e., palm, bodies, grippers, and joints, are disjoint in their
interiors. (In configurations where the joints bend, some parts may
overlap with the joints.) In the following we describe these parts in
details.

%% \begin{definition}[Palm]
%% A \emph{palm} is an extrusion of a facet of $P$.
%% \end{definition}

%% \begin{definition}[Finger]
%% Fingers are extensions of the \emph{palm}. Each \emph{finger} consists
%% of two polyhedral parts, namely, \emph{body} and \emph{gripper} and
%% the semi-rigid joint between the \emph{body} and the \emph{gripper}.
%% \end{definition}

Let $G$ denote a snapping fixture made of a palm, $k$ fingers,
$F_1,F_2,\ldots,F_k$, and corresponding joints. The palm is an
$\alpha_p$-extrusion of a facet $f_p$ of $P$. (The value of $\alpha_p$
is discussed below.) Consider a specific finger $F=F_i$ of $G$. The
body of $F$ is defined by one of the neighboring facets of $f_p$,
denoted $f_b$. The gripper of $F$ is defined by one of the neighboring
facets of $f_b$, denoted $f_g$, $f_g\neq f_p$. Let $e_{pb}$ denote the
common edge of $f_p$ and $f_b$, and let $e_{bg}$ denote the common
edge of $f_b$ and $f_g$. Let $q_b$ denote the quadrilateral defined by
the vertices incident to $e_{pb}$ and $e_{bg}$. Note that in some
degenerate cases $e_{pb}$ and $e_{bg}$ are incident to a common
vertex; in these cases $q_b$ is a triangle. The body is an
$\alpha_b$-extrusion of $q_b$. (The value of $\alpha_b$ is also
discussed below.) Let $v$ denote the cross product of the vector that
corresponds to $e_{bg}$ and the normal to the plane containing $f_g$
of length $\alpha_g$ (discussed below). Let $q_g$ denote the
quadrilateral defined by the two vertices incident to $e_{bg}$ and
their translations by $v$. The gripper is an $\alpha_g$-extrusion of
$q_g$. The axis of the joint that connects the palm and the body of
$F$ coincides with $e_{pb}$ and the axis of the joint that connects
the body of $F$ with its gripper coincides with $e_{bg}$.  The value
$\alpha_p$ and the values $\alpha_b$ and $\alpha_g$ for each finger
determine the trade-off between the strength and flexibility of the
joints.\footnote{Typically, these values are identical.} They depends
on the material and shape of the fixture. In our implementation they
are defined by the user.

\newlength{\intextsepSaved}\setlength\intextsepSaved{\intextsep}%
\newlength{\columnsepSaved}\setlength\columnsepSaved{\columnsep}%
\setlength{\intextsep}{-5pt}%
\setlength{\columnsep}{0pt}%
\begin{wrapfigure}[8]{R}{3.2cm}
  \begin{tikzpicture}[node distance=5cm]
    \node (img) {\includegraphics[width=3.0cm]{../figs/fix_coll_simple.png}};
    \node [inner sep=-0pt](bs) at (-0.85, -0.4) {$f_{b_2}$};
    \node [inner sep=0pt](gs) at (0.4, 0.0) {$f_{g_1}$};
    \node [inner sep=0pt](fs) at (1.4, 1.5) {$f$};
    \coordinate (be) at (0.1,-0.4);
    \coordinate (ge) at (-0.5,0.0);
    \coordinate (fe) at (1.2,0.5);
    \draw [thick,-{Stealth[scale=1.5]}] (bs)--(be);
    \draw [thick,-{Stealth[scale=1.5]}] (gs)--(ge);
    \draw [thick,-{Stealth[scale=1.5]}] (fs)--(fe);
  \end{tikzpicture}
\end{wrapfigure}
For a complete view of a workpiece and its snapping fixture consider
Figure~\ref{fig:wrap}. Observe that both the palm and the fingers of
the fixture in the figure differ from the formal definitions
above. The differences stem from practical reasons. In particular, the
parts in the figure have smaller volumes, which (i) reduces
fabrication costs, and (ii) resolves collision between distinct
fingers. In some degenerate cases, such as the one depicted to the
right, distinct fingers could have collided. In the figure, the base
facet of the gripper of one finger, $f_{g_1}$, coincides with $f$, a
facet of the workpiece. Likewise, the base facet of the body of the
other finger, $f_{b_2}$, also coincides with $f$. Avoiding collision
is achieved by simultaneously shrinking the base facets $f_{g_1}$ and
$f_{b_2}$.  Now, the gripper grips only the tip of $f$ and the body is
stretching only on a slim part of the workpiece facet. As another
example, consider the body of a finger depicted in
Figure~\ref{fig:wrap}\subref{fig:fix1:t}, which is the
$\alpha_b$-extrusion of the quadrilateral $q_b$ in the figure. It is
defined by two points that lie in the interior of $e_{pb}$ and two
points that lie in the interior of $e_{bg}$, as opposed to the formal
definition above, where the endpoints of $e_{pb}$ and $e_{bg}$ define
the corresponding formal quadrilateral. Also, in reality, parts, and
in particular joints, are not fabricated separately, and the entire
fixture is made of the same flexible material. Instead of rotating
about the the joint axes, the entire fingers bend. The differences,
though, have no effect on the correctness of the proofs and algorithms
(which adhere to the formal definitions) presented in the sequel.
These structural changes, the structures of the joints, and the
extrusion values, merely determine the degree of flexibility and
strength of the fixture; see Section~\ref{ssec:future:strength} and
Section~\ref{ssec:future:flexibility}.%
\setlength\intextsep{\intextsepSaved}%
\setlength\columnsep{\columnsepSaved}%

 \begin{figure}[!ht]
   \centering\subfloat[]{\label{fig:fix1:o}
     \begin{tikzpicture}
       \node (img) {\includegraphics[width=0.3\linewidth]{../figs/fix_overall.png}};
       \node [](P) at (1.1,-1.1) {\textcolor{color2!50!black}{$P$}};
       \node [](ps) at (-0.9, 1.5) {\textcolor{color1!50!black}{Palm}};
       \node [](fs) at (0.6, 1.5) {\textcolor{color1!50!black}{Fingers}};
       \coordinate (pe) at (-0.9,0.);
       \draw [thick,-{Stealth[scale=1.5]}] (ps)--(pe);
       \coordinate (fe1) at (0.6,0.05);
       \coordinate (fe2) at (-0.7,0.6);
       \coordinate (fe3) at (-0.8,-0.6);
       \draw [thick,-{Stealth[scale=1.5]}] (fs)--(fe1);
       \draw [thick,-{Stealth[scale=1.5]}] (fs) to[out=-90,in=0](fe2);
       \draw [thick,-{Stealth[scale=1.5]}] (fs) to[out=-90,in=0](fe3);
   \end{tikzpicture}}
   \subfloat[]{\label{fig:fix1:f}
     \begin{tikzpicture}[node distance=10cm]
       \node (img) {\includegraphics[width=0.3\linewidth]{../figs/fix_finger.png}};
       %% \node [](C) at (0.2, 0.95) {$v$};
       %% \node [](D) at (0, 1.65) {Gripper base facet};
       %% \node [](D) at (0, 1.32) {opposite normal};
       %
       \node [](gs) at (0.2, 1.5) {\textcolor{color1!50!black}{Gripper}};
       \node [](bs) at (-0.95, 1.5) {\textcolor{color1!50!black}{Body}};
       %% \begin{scope}[fill opacity=0.5,rounded corners]
       %%   \node [fill=color2!50!white](gs) at (-0.5, 0.9) {Gripper};
       %%   \node [fill=color2!50!white](bs) at (-0.5, 0.1) {Body};
       %% \end{scope}
       \coordinate (ge) at (0.8,0.95);
       \coordinate (be) at (0.8,0.1);
       \draw [thick,-{Stealth[scale=1.5]}] (gs)to[out=-90,in=180](ge);
       \draw [thick,-{Stealth[scale=1.5]}] (bs)to[out=-90,in=180](be);
   \end{tikzpicture}}
   \subfloat[]{\label{fig:fix1:t}
     \begin{tikzpicture}
       \node (img) {\includegraphics[width=0.3\linewidth]{../figs/fix_finger_top.png}};
       %% \node [](B) at (0.76, 0.85) {$q_{q}$};
       %% \node [](A) at (0.92, 0.4) {$q_{b}$};
       \node [fill=color2!50!white,inner sep=1pt](epbs) at (-0.63, -0.2) {\textcolor{green!50!black}{$e_{pb}$}};
       \coordinate (epbe) at (-0.3,-0.95);
       \draw [thick,-{Stealth[scale=1.5]}] (epbs)--(epbe);
       %
       \node [fill=color2!50!white,inner sep=1pt](ebgs) at (0.4, -0.1) {\textcolor{green!50!black}{$e_{bg}$}};
       \coordinate (ebge) at (0.1,0.55);
       \draw [thick,-{Stealth[scale=1.5]}] (ebgs)--(ebge);
   \end{tikzpicture}}
  \setlength{\abovecaptionskip}{5pt}
  \setlength{\belowcaptionskip}{-5pt}
   \caption[]{Three different views of a polyhedron $P$ (blue) and a
     snapping fixture of $P$ (orange).}
   \label{fig:wrap}
\end{figure}

% -----------------------------------------------------------------------------
\subsection{The Configuration Space}
\label{ssec:terms:configuration-space}
% -----------------------------------------------------------------------------
The workpiece and its snapping fixture form an assembly. Each joint in
the fixture connects two parts; it enables the rotation of one part
with respect to the other about an axis.  Each joint adds one degree
of freedom (DOF) to the configuration space of the assembly. Thus,
the configuration space of the assembly (which is the configuration
space of the fixture, assuming the workpiece is stationary) has $6+2k$
DOFs, where $k$ indicates the number of fingers.

In our context, the workpiece and its snapping fixture are considered
assembled, if they are \emph{infinitesimally inseparable}. When two
polyhedra are infinitesimally inseparable, any linear motion applied
to one of the polyhedra causes a collision between the polyhedra interiors.

\begin{definition}[Serving configuration]
  The workpiece and the fixture are in the \emph{serving configuration}
  if (i) they are separated (that is, they are arbitrarily far away
  from each other), and (ii) there exists a vector $v$, such that when
  the fixture is translated by $v$, as a result of some force applied
  in the direction of $v$, exploiting the flexibility of the joints of
  the fixture, the workpiece and the fixture become assembled.
\end{definition}
%% \efi{The above may not deserve a definition block, but you can keep it
%%   for now. Also, consider introducing another configuration, where the
%%   fixture is already in place, but its grippers are bent. Then, use it
%%   below in the ``Flexibility'' subsubsection.}

When the workpiece and its snapping fixture are separated, the fixture
can be transformed without colliding with the workpiece to reach the
serving configuration.

\begin{figure}[ht]
  \centering%
  \subfloat[][]{\label{fig:fix2:P}%
    \includegraphics[width=0.2\linewidth]{../figs/polyhedron.png}}\hfil
  \subfloat[][]{\label{fig:fix2:G}%
    \includegraphics[width=0.2\linewidth]{../figs/polyhedron_fix.png}}\hfil
  \subfloat[][]{\label{fig:fix2:G-wrap-P}%
    \includegraphics[width=0.2\linewidth]{../figs/polyhedron_asm.png}}\hfil
  \setlength{\abovecaptionskip}{5pt}
  \setlength{\belowcaptionskip}{-5pt}
  \caption[]{(\subref*{fig:fix2:P}) $P$ (\subref*{fig:fix2:G}) $G$
    (\subref*{fig:fix2:G-wrap-P}) $P$ and $G$ are assembled}
  \label{fig:fixture}
\end{figure}

% -----------------------------------------------------------------------------
\subsection{Spreading Degree}
\label{sssec:terms:spreading-degree}
% -----------------------------------------------------------------------------
\setlength{\intextsep}{0pt}%
\begin{wrapfigure}[6]{r}{2cm}
  \centering%
  \includegraphics[trim={1cm 0 0 0},width=2cm]{../figs/sphere_like_2.png}
\end{wrapfigure}
%% Given a polyhedral representation $P$ of a workpiece, we list some
%% properties that a polyhedral representation $G$, of a snapping fixture
%% of $P$, possesses.
The \emph{spreading degree} is the number of facets involved in the
definition of a finger.  In this paper we restrict ourselves to
snapping fixtures that have fingers with spreading degree two, which
means that the body of every finger is based on a single facet of $P$.
Every finger (the body and the gripper) stretches over two facets of
$P$. Naturally, fingers with a higher spreading-degree reach
further. The figure above depicts an icosahedron that does not admit a
valid fixture with spreading degree two.%
\setlength\intextsep{\intextsepSaved}%

% -----------------------------------------------------------------------------
\subsection{Fixture Planning}
\label{ssec:terms:planning}
% -----------------------------------------------------------------------------
The basic objective of our fixture algorithm is obtaining fixtures
with the minimal number of fingers. Our planner is of the exhaustive
type. As explained in Section~\ref{sec:algorithms}, it examines many
different possible candidates of fingers, before it reaches a
conclusion. As aforementioned, the planner generates fixtures of
spreading degree two. Extending the planner to enable the generation
of fixtures with an increased spreading degree (without further
modifications), will directly increase the search space exponentially.
% , and likely render the procedure infeasible in many cases.
% -----------------------------------------------------------------------------
\subsection{Properties}
\label{ssec:terms:planning}
% -----------------------------------------------------------------------------
When a facet $f$ of the workpiece partially coincides with a facet of the
fixture, the workpiece cannot translate in any direction that forms an
acute angle with the (outer) normal to the plane containing $f$
(without colliding with the fixture). This set of blocking directions
comprises an open unit hemisphere denoted as $h(f)$. Similarly,
$\calH(\calF) = \{h(f)\,|\,f \in \calF\}$ denotes the mapping from a
set of facets to the set of corresponding open unit hemispheres; see,
e.g., \cite{shb-spspm-17}. Let $\calF'$ denote the set of facets of
the workpiece that are coincident with facets of the fixture in some
fixed configuration. If the union of all blocking directions covers the
unit sphere in that configuration, formally stated $\SOtwo = \bigcup
\calH(\calF')$, then the workpiece cannot translate at all.

Let $\calF$ denote the set of all facets of $G$. Let $\calF_{P}$
denote the singleton that consists of the base facet of the palm of
$G$, and let $f_{b_{i}}$ and $f_{g_{i}}$, $1\leq i\leq k$, denote the
base facet of the body and the base facet of the gripper,
respectively, of the $i$-th finger of $G$, where $k$ indicates the
number of fingers. Let $\calF_{B}=\{f_{b_{i}}\,|\,1\leq i\leq k\}$ and
$\calF_{G}=\{f_{g_{i}}\,|\,1\leq i\leq k\}$ denote the set of the base
facets of the body parts of the fingers of $G$ and the set of the base
facets of the gripper parts of the fingers of $G$, respectively. Let
$\calF_{PBG}$ denote the set of all base facets of the parts of $G$,
that is $\calF_{PBG} = \calF_{P} \cup \calF_{B} \cup \calF_{G}$.  Let
$\calF_{PB}$ denote the set of all base facets of the parts of $G$
excluding the base facets of the grippers, that is,
$\calF_{PB} = \calF_{P} \cup {\calF}_{B}$.

\begin{corollary}\label{corollary:valid-fixture}
  If the fixture resists any linear force applied on the workpiece
  while in the assembled configuration and there exists a collision
  free path (in the configuration space) between some well separated
  configuration and the assembled configuration then our fixture is
  valid. We relax the second condition for practical reasons and
  instead of requiring an arbitrarily long path, we require a path of
  infinitesimal length. Formally we get:

  \begin{enumerate}
  \item $\SOtwo=\bigcup \calH(\calF_{PBG})$.
  \item $\SOtwo\neq\bigcup \calH(\calF_{PB})$.
  \end{enumerate}
\end{corollary}
If the second condition holds, a serving state exists (assuming the
flexibility of the joints cancels out the problem induced by the
presence of the grippers).

A candidate finger of an input polyhedron $P$ is a valid finger of at
least one possible fixture $G$ of $P$.

\begin{observation}\label{observation:candidate-fingers}
  The number of candidate fingers of an input polyhedron $P$ is linear
  in the number of vertices of $P$.
\end{observation}

\begin{proof}
  Let $e$ be an edge of $P$ and let $f_{e}$ and $f_{e}'$ be the two
  faces incident to $e$. Two fingers can be built on $e$. The base
  facet of the body and the base facet of the gripper of one finger
  coincides with $f_{e}$ and $f_{e}'$, respectively. In order to
  construct the other finger, the roles of these facets exchange; that
  is, the base facet of the body and the base facet of the gripper
  coincides with $f_{e}'$ and $f_{e}$, respectively. Every candidate
  finger is built on a single edge. Thus, the number of candidate
  fingers is at most $2|E|$. From Euler's formula we know that the
  number of edges in a polyhedron is at most $3n-6$, where $n$ is the
  number of vertices of the polyhedron. Thus, the number of candidate
  fingers is at most $6n-12$.
\end{proof}

\begin{theorem}\label{theorem:upper-bound}
  Four fingers are always sufficient and sometimes necessary for a valid
  snapping fixture.
  %% The minimal number of fingers of valid fixtures of any given polyhedron
  %% is four. This bound is tight.
\end{theorem}

\begin{observation}\label{observation:one-finger}
  A fixture with only one finger does not exist.
\end{observation}

The proofs of Theorem~\ref{theorem:upper-bound} and of
Observation~\ref{observation:one-finger} appear in the supplementary
material; see also~\cite{aXivr}; Nevertheless, a polyhedron that
admits the lower bound is depicted in Figure~\ref{fig:special}. While
the lower bound is tight, there exists a polyhedron that has a
snapping fixture that has only two fingers; see Figure~\ref{fig:fixture}

\begin{figure}[ht]
  \centering%
  \subfloat[][]{\label{fig:special:1}%
    \includegraphics[width=0.33\linewidth]{../figs/s_1.png}}\hfil
  \subfloat[][]{\label{fig:special:2}%
    \includegraphics[width=0.33\linewidth]{../figs/s_2.png}}\hfil
  \subfloat[][]{\label{fig:special:3}%
    \includegraphics[width=0.33\linewidth]{../figs/s_3.png}}\hfil
  \caption[]{Different views of the specialhedron---a polyhedron that has one valid fixture with four fingers.}
  \label{fig:special}
\end{figure}
