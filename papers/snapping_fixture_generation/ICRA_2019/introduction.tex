% =============================================================================
\section{Introduction}
\label{sec:intro}
% =============================================================================
A fixture is a device that holds a part in place. There are many types
and forms of fixtures; they range from modular fixtures synthesized on
a lattice to fixtures generated to suit a specific part. A
fixture possess some grasp characteristics. For example, a grasp with
complete restraint prevents loss of contact, prevents any motion, and
thus may by considered secure. Two primary kinematic restraint
properties are \emph{form closure} and \emph{force
  closure}~\cite{pt-g-16}.  Both properties guarantee maintenance of
contact under some conditions. However, the latter relies on contact
friction; therefore, achieving force closure typically requires fewer
contacts than achieving form closure.  Fixtures with complete
restraint are mainly used in manufacturing processes where preventing
any motion is critical. Other types of fixtures can be found anywhere,
for example, in the kitchen where a hook holds a cooking pan, or in
the office where a pin and a bulletin board hold a paper still. This
paper deals with a specific problem in this area; here, we are given a
rigid object, referred to as the \emph{workpiece}, and we seek for an
automated process that designs a semi-rigid object, referred to as the
holding \emph{fixture}, such that, starting at a configuration where
the workpiece and the holding fixture are separated, they can be
pushed towards each other, applying a linear force and exploiting the
mild flexibility of the fixture, into a configuration where both the
workpiece and the fixture are inseparable as rigid bodies. Without
additional computational effort, a hook, a nut, or a bolt can be added
to a base part of the fixture (referred to as the \emph{palm} and defined
in Section~\ref{ssec:terms:structure}). This results in a generic fixture
that can be utilized in a larger system. Another advantage of the
single-component flexible fixture is that it can easily be 3D-printed.
We have 3D-printed several workpieces and suitable fixtures that our
system has automatically generated.
% see Appendix~\ref{sec:appendix:printing}.
The objective of the algorithm is obtaining snapping fixtures, the
skeleton of which has low complexity; for a precise definition see
Section~\ref{ssec:terms:planning}. With additional care that also
accounts for properties of the material used to produce the fixtures,
the smallest or lightest possible fixture can be synthesized, for a
given workpiece. This can (i) expedite the production of the fixture
using, e.g., additive manufacturing, (ii) minimize the weight of the
produced fixture, and (iii) maximize the exposed area of the boundary
of the workpiece when held by the fixture.

% -----------------------------------------------------------------------------
\subsection{Background}
\label{ssec:intro:background}
% -----------------------------------------------------------------------------
Form closure has been studied since the 19th century. Early results
showed that at least four frictionless contacts are necessary for
grasping an object in the plane, and seven in 3D space. Specifically,
it has been shown that four and seven contacts are necessary and
sufficient for the form-closure grasp of any polyhedron in the 2D and
3D case, respectively~\cite{mnp-gg-90, mss-esmpg-87}.

Automatic generation of various types of fixtures, and in particular,
the synthesis of form-closure grasps, are the subjects of a diverse
body of research. Brost and Goldberg~\cite{bg-casmf-94} proposed
a complete algorithm for synthesizing modular fixtures of polygonal
workpieces by locating three pegs (locators), and one clamp on a
lattice. Their algorithm is complete in the sense that it examines all
possible fixtures for an input polygon. Their results were obtained by
generating all configurations of three locators coincident to three
edges, for each triplet of edges in the input polygon. For each such
configuration, the algorithm checks whether \emph{form closure} can be
obtained by adding a single clamp. Our work uses a similar iteration
strategy to obtain all possible configurations. In subsequent work
Zhuang, Goldberg, and Wong~\cite{zk-esmf-96} showed that there exists
a non-trivial class of polygonal workpieces that cannot be held in
form closure by any fixture of this type (namely, a fixture that uses
three locators and a clamp). They also considered fixtures that use
four clamps, and introduced two classes of polygonal workpieces that
are guaranteed to be held in form closure by some fixture of this
type.  Some of our results are reminiscent of their results. Wallack
and Canny~\cite{wc-pmhf-97} proposed another type of fixture called
the vise fixture and an algorithm for automatically designing such
fixtures. The vise fixture includes two lattice plates mounted on the
jaws of a vise and pegs mounted on the plates. Then, the workpiece is
placed on the plates, and \emph{form closure} is achieved by
activating the vise and closing the pins from both sides on the
workpiece. The main advantage in this type of fixture is its
simplicity of usage. Brost and Peters~\cite{bp-adfap-98} extended
the approach exploited in~\cite{bg-casmf-94} to three dimensions. They
provided an algorithm that generates suitable fixtures for
three-dimensional workpieces.  Wagner, Zhuang, and
Goldberg~\cite{wzg-ffpsm-95} proposed a three-dimensional
seven-contact fixture device and an algorithm for planning
form-closure fixtures of a polyhedral workpiece with pre-specified
pose. A summary of the studies in the field of flexible fixture design
and automation conducted in the last century can be found
in~\cite{bz-ffdar-01, bk-rgm-01}. Subsequent works studied other
types of fixtures and provided algorithms for computing them, for
example, unilateral fixtures~\cite{ggbzk-ufsmp-04}, which are used to
fix sheet-metal workpieces with holes. Other studies focused on
grasping synthesis algorithms with autonomous robotic \emph{fingers},
where a single robotic hand gets manipulated by motors and being used
to grasp different workpieces; an overview of such algorithms can be
found in ~\cite{seb-oogsa-12}. A common dilemma for all the grasping
and fixture design algorithms is defining and finding the optimal
grasp. Several works, e.g.,~\cite{lwxl-qfgsf-04}
and~\cite{rpr-goscc-13}, discuss such quality functions and their
optimizations.

% -----------------------------------------------------------------------------
\subsection{Our Results}
\label{ssec:intro:results}
% -----------------------------------------------------------------------------
We define certain properties that a pair of a workpiece and its
snapping fixture should possess. Formally, we are given a simple and
closed polyhedron $P$ of complexity $n$ that represents a workpiece.
We introduce an algorithm that determines whether a simple and closed
polyhedron $G$ that represents a fixture exists, and if so, it
constructs it in $O(n^5)$ time. We also provide an efficient and
robust implementation of this algorithm. In addition, we present two
practical cases that utilze our algotithm and its implementation: One
is the generation of a snapping fixture that mounts a device to an
unmanned aerial vehicle (UAV), such as a drone. The other is the
generation a snapping fixture that mounts a precious stone to a jewel,
such as a ring. The common objective in both cases is, naturally, the
firm holding of the workpiece. In the first case, we are interested in
a fixture with minimal weight. In the second case we are interested in
a fixture with minimal volume, such that it minimaly obscures the
precious stone. Note that the obtained fixture prevents any linear
motion, but does not necessarily prevent angular motion; hence, the
held workpiece does not posses the \emph{form closure} property per
se. Handling angular motion is left for the future; see
Section~\ref{ssec:future:form-closure}.
%% \footnote{Among all fixtures we have synthesized we have rarely
%%   encountered fixture that do not posses \emph{form closure}.
%%   Investigating how to assure form closure is left for the future.}
For lack of space, we refer the interested reader to
complimentary theorems and their proofs~\ref{}.

% -----------------------------------------------------------------------------
\subsection{Outline}
\label{ssec:intro:outline}
% -----------------------------------------------------------------------------
The rest of this paper is organized as follows. Terms and definitions
for our snapping fixtures are provided in
Section~\ref{sec:terms-properties}. Theoretical bounds and properties
are provided also in Section~\ref{sec:terms-properties}. The synthesis
algorithm is described in Section~\ref{sec:algorithms} along with the
analysis of its complexity. Two applications are presented in
Section~\ref{sec:cases}. Finally, we point out some limitations of our
planer and suggest future research in Section~\ref{sec:future}.

% We report on experimental results in Section \ref{sec:experiments}.
