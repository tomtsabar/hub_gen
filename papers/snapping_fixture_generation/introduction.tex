% =============================================================================
\section{Introduction}
\label{sec:intro}
% =============================================================================
A fixture is a device that holds a part in place. There are many types
and forms of fixtures; they range from modular fixtures synthesized on
a lattice to flexible fixtures generated to suit a specific part. A
fixture possess some grasp characteristics. For example, a grasp with
complete restraint prevents loss of contact, prevents any motion, and
thus may by considered secure. Two primary kinematic restraint
properties are \emph{form closure} and \emph{force
  closure}~\cite{pt-g-16}.  Both properties guarantee maintenance of
contact under some conditions; however, the latter relies on contact
friction; therefore, achieving force closure typically require fewer
contacts than achieving form closure.  Fixtures with complete
restraint are mainly used in manufacturing processes where preventing
any motion is critical. Other types of fixtures can be found anywhere,
for example, in the kitchen where a hook holds a cooking pan, or in
the office where a pin and a bulletin board hold a paper still. This
paper deals with a specific problem in this area; here, we are given a
rigid object, referred to as the \emph{workpiece}, and we seek for an
automated process that designs a semi-rigid object, referred to as the
holding \emph{fixture}, such that, starting at a configuration where
the workpiece and the holding fixture are separated, they can be
pushed towards each other, applying a linear force and exploiting the
mild flexibility of the fixture, into a configuration where both the
workpiece and the fixture are inseparable as rigid bodies. Without
additional computational effort, a hook, a nut, or a bolt can be added
to a base part of the fixture (defined in the introduction and
referred to as the \emph{palm}). This results in a generic fixture
that can be utilized in a larger system. Another advantage of the
single-component flexible fixture is that it can easily be 3D-printed.
We have 3D-printed several workpieces and suitable fixtures that our
system has automatically generated; see
Appendix~\ref{sec:appendix:printing}.  The objective of the algorithm
is minimizing the generated-fixture complexity. With additional care
that also accounts for properties of the material used to produce the
fixtures, the smallest or lightest possible fixture can be generated,
for a given workpiece, which maximizes the exposed area of the
boundary of the workpiece.

% -----------------------------------------------------------------------------
\subsection{Background}
\label{ssec:intro:background}
% -----------------------------------------------------------------------------
Form closure has been studied since the 20th century. Early results
showed that at least four frictionless contacts are necessary for
grasping an object in the plane, and seven in 3D space. Specifically,
it has been shown that four and seven contacts are necessary and
sufficient for the form-closure grasp of any polyhedron in the 2D and
3D case~\cite{mnp-gg-90, mss-esmpg-87}.

Automatic generation of various types of fixtures, and in particular,
the synthesis of form-closure grasps, are the subjects of a diverse
body of research. Brost and Goldberg~\cite{bg-casmf-94} have proposed
a complete algorithm for synthesizing modular fixtures of polygonal
workpieces by locating three pegs (locators), and one clamp on a
lattice. Their algorithm is complete in the sense that it examines all
possible fixtures for an input polygon. Their results were obtained by
generating all configurations of three locators coincident to three
edges, for each triplet of edges in the input polygon. For each such
configuration, the algorithm checks whether \emph{form closure} can be
obtained by adding a single clamp. Our work uses a similar iteration
strategy to obtain all possible configurations. In subsequent work
Zhuang, Goldberg, and Wong~\cite{zk-esmf-96} showed that there exists
a non trivial class of polygonal workpieces that cannot be held in
form closure by any fixture of this type (namely, a fixture that uses
three locators and a clamp). They also considered fixtures that use
four clamps, and introduced two classes of polygonal workpieces that
are guaranteed to be held in form closure by some fixture of this
type.  Some of our results are reminiscent of their results. Wallack
and Canny~\cite{wc-pmhf-97} proposed another type of fixture called
the vise fixture and an algorithm for automatically designing such
fixtures. The vise fixture includes two lattice plates mounted on the
jaws of a vise and pegs mounted on the plates. Then, the workpiece is
placed on the plates, and \emph{form closure} is achieved by
activating the vise and closing the pins from both sides on the
workpiece. The main advantage in this type of fixture is its
simplicity of usage. Brost and Peters~\cite{bp-adfap-98} have extended
the approach exploited in~\cite{bg-casmf-94} to three dimensions. They
have provided an algorithm that generates suitable fixtures for
three-dimensional workpieces.  Wagner, Zhuang, and
Goldberg~\cite{wzg-ffpsm-95} have proposed a three-dimensional
seven-contact fixture device and an algorithm for planning
form-closure fixtures of a polyhedral workpieces with pre-specified
pose. A summary of the studies in the field of flexible fixture design
and automation conducted in the last century can be found
here~\cite{bz-ffdar-01, bk-rgm-01}. Subsequent works studied other
types of fixtures and provided algorithms for computing them, for
example, unilateral fixtures~\cite{ggbzk-ufsmp-04}, which are used to
fix sheet-metal workpieces with holes. Other studies focused on
grasping synthesis algorithms with autonomous robotic \emph{fingers},
where a single robotic hand get manipulated by motors and being used
to grasps different workpieces; an overview of such algorithms can be
found in ~\cite{seb-oogsa-12}. A common dilemma for all the grasping
and fixture design algorithms is defining and finding the optimal
grasp. Several works, e.g.,~\cite{lwxl-qfgsf-04}
and~\cite{rpr-goscc-13}, discuss such quality functions and their
optimizations.

% -----------------------------------------------------------------------------
\subsection{Our Results}
\label{ssec:intro:results}
% -----------------------------------------------------------------------------
We define certain properties that a pair of a workpiece and its
flexible fixture may possess, and prove two theorems related to such
pairs.  Formally, we are given a simple and closed polyhedron $P$ of
complexity $n$ that represents a workpiece.  We introduce an algorithm
that determines whether a simple and closed polyhedron $G$ that
represents a fixture exists, and if so, it constructs it in $O(n^5)$
time. We also provide an efficient and robust implementation of this
algorithm. In addition, we suggest an algorithm that solves the same
problem in $O(n^3)$ time.  However, this algorithm is more complex
and, thus, more difficult to implement. Note that an obtained fixture
prevents any linear motion but does not necessarily prevent angular
motion; hence, it does not posses the \emph{form closure} property per
se.

% -----------------------------------------------------------------------------
\subsection{Outline}
\label{ssec:intro:outline}
% -----------------------------------------------------------------------------
The rest of this paper is organized as follows.  Terms and definitions
used by the actual planners are provided in Section~\ref{sec:terms}.
Theoretical bounds and properties are provided in
Section~\ref{sec:properties}. The two algorithms are described in
Section~\ref{sec:algorithms} along with the analysis of their
complexities. We conclude with ideas for future research in
Section~\ref{sec:future}.

% We report on experimental results in Section \ref{sec:experiments}.
