% =============================================================================
\section{Algorithms}
\label{sec:algorithms}
% =============================================================================
\begin{algorithm}[htb]
  \caption{Fixture generation in $O(n^{5})$ time}
  \label{alg:fixture gen}
  \begin{algorithmic}[1]
    \Require A simple polyhedron $P$
    \Ensure A fixture $G$ for $P$, if there exists one, with minimum number
    of fingers.
    \State facetToFingersMapping $\leftarrow$ GetAllPossibleFingerForEveryFacet(P)
    \State best $\leftarrow$ null
    \ForAll{facet $f_{i}$}
      \ForAll{subset of fingers smaller then 5 in facetToFingersMapping{[}i{]}}
        \If {CheckValidity(subset,$f_{i}$) == true}
          \State best $\leftarrow$ FindBest(best, subset)
        \EndIf
      \EndFor
    \EndFor
    \State return best
  \end{algorithmic}
\end{algorithm}

\begin{func}{GetAllPossibleFingerForEveryFacet}
  The function iterates over all half-edges of $P$, every edge $e$
  connects two facets---$f_{i}$ and $f_{j}$. For every neighbour
  $f_{k}$ of $f_{j}$ the finger built by $f_{j}$ as the body facet and
  $f_{i}$ as the gripper is added the mapping
  facetToFingersMapping{[}k{]}.
\end{func}

\begin{func}{CheckValidity}
  The function checks whether a set of open unit hemispheres corresponding
  to the geomteric embedding of a given set of fingers form a covering
  set, and if the same set taken out the gripper corresponding open
  unit hemispheres does not form a covering set. The last check is if
  all the fingers can be built valid. The covering set checking is
  done by following the steps from Lemma 1-4, the valid building
  checking is done by calculate the trigonometric formulas with given
  parameters.
\end{func}

\begin{func}{FindBest}
  The first tiebreaker between two fixtures is the number of fingers;
  the second tiebreaker is an optimal optimization function, for
  example, the minimum amount of force applied in any direction
  necessary to separate $G$ and $P$ when they are in the assembled
  state might be a good second tiebreaker.
\end{func}

% -----------------------------------------------------------------------------
\subsection{Complixity}
\label{ssec:algorithms:complixity1}
% -----------------------------------------------------------------------------
\begin{enumerate}
\item GetAllPossibleFingerForEveryFacet: $O(n^{2})$ There is a
  linear sum of half-edges in $P$, the algorithm iterates over all
  neighbours of all the facets of one side of the half-edge (for each
  half-edge). The number of neighbours is linear therefore the overall
  time complexity of the function is $O(n\cdot n))=O(n^{2})$.

\item CheckValidity: $O(1\cdot n^{5})$ The time complexity of
  CheckValidity function is $O(1)$ . That is because the size of the
  input of CheckValidity is bounded, a finger's encoding has fixed
  size and CheckValidity gets at most 4 fingers. For every possible
  palm CheckValidity get called once per subgroup of fingers of size
  less then 5---this sum up to $O(n(\begin{pmatrix}n^
    2\end{pmatrix}+\begin{pmatrix}n^ 3\end{pmatrix}+\begin{pmatrix}n^
    4\end{pmatrix}))=O(n\cdot n^{4})=O(n^{5})$

\item FindBest: $O(1\cdot n^{5})$ The time complexity of
  FindBest function is $O(1)$ and it get called at most
  $O(n^{5})$, the same explanation for CheckValidity works
  well for this function too, the input size is bounded.
\end{enumerate}
The algorithm runs in $O(n^{2}+n^{5}+n^{5})=O(n^{5})$ time.

% -----------------------------------------------------------------------------
\subsection{Further analysis}
\label{ssec:algorithms:analysis}
% -----------------------------------------------------------------------------
\begin{theorem*}{Kuratowski's theorem~\cite{t-kt-81}}
A graph is planar if and only if it does not contain a subgraph that
is a subdivision of $K_{5}$ or of $K_{3,3}$.
\end{theorem*}

\begin{lemma}
For each triplet of fingers there exists at most two distinct palms that holds that each finger in the triplet is a candidate finger for the palm.
\end{lemma}

\begin{proof}
Let $F_3=\{fi_1, fi_2, fi_3\}$ be a set of three fingers.
The claim is
negated to assume that there exists three different palms where each
finger in $F_3$ is a candidate finger for each of the palms.
Hence, each facet
of the three palms is a neighbor of each one of the three body facets of the three fingers, and the other way around, each one of the three
body facets is a neighbour of each one of the three palms facets. When
considering the dual graph to the graph of the polyhedron, the facets
become vertices and neighbours facets get connected by an edge. In
this dual graph, the description above of the neighbouring relations
between the three body facets and the three palms facets describes
$k_{3,3}$. The graph of the simple input polyhedron is a planar graph and therfore its dual graph is also planar. This results in a contradiction since
Kuratowski's theorem states that a planar graph cannot contains subdivision of $k_{3,3}$ but the graph described is both planar and contains $k_{3,3}$.
Therefore for each triplet of fingers there exists at most two distinct palms that holds that each finger in the triplet is a candidate finger for the palm.
\end{proof}

\begin{corollary}
The algorithm described earlier, activated only on subgroups of
fingers of size 3 and 2 runs in $O(n^{3})$ time complexity.
\end{corollary}

\begin{proof}
In the time complexity analysis of Algorithm 1, we assumed that
iterating over all subgroups of fingers of size 3 for each palm
separately, cost $O(n \cdot n^{3})$ in running time. When considering a fine grained analysis, we notice that each triplet of fingers
appears as candidate for most at two distinct palms, the meaning of this observation is that while iterating over all possible fixtures that contain three
fingers, each triplet of fingers is considered at most two times, therefore the total time complexity is bounded by twice the number of fingers. Hence the total time complexity is $O((2 \cdot n^{3}) = O(n^{3})$.
The previous time complexity analysis considering iteration over fixtures that contains two fingers sums up to $O(n \cdot n^{2}) =
O(n^3)$, So when considering both types of fixtures we get a total of $O(n^3 + n^3) = O(n^3)$ time complexity.
\end{proof}

\begin{lemma}
Let $S$ be a set of open unit semicircles that covers $\SOone$, if a
$|S|\geq5$ then there exists $R \subset S$, $|R|=3$ and $R$ covers
$\SOone$.
\end{lemma}

\begin{proof}
Let $S_{min}\subseteq S$ be the minimal subset of $S$ that covers
$\SOone$. As proved in Lemma 2, $|S_{min}|\in{\{3,4\}}$, it is
derived from the lemma is that if $|S_{min}|=4$, then the anti-polaric
open unit semicircle of the open unit semicircle first to be chosen,
must be included in $S_{min}$, this state holds for every possibly
first chosen open unit semicircle. Because every open unit semicircles
can be equaly chosen as the first, the case where
$|S_{min}|=4$ is the case where the anti-polaric open unit semicircles
of every open unit semicircle is in $S_{min}$. Therefore if $|S|\geq5$
and assuming $|S_{min}| = 4$, the subset $R'$ can be built by taking
$S_{min}$ and adding one open unit semicircle from $S\setminus S_{min}$ to the set, now
$|R'|=5$. Notice that any set of open unit semicircles can be divided
into equivalence classes of anti-polaric pairs, therefore in a size 5 set, there exists an
open unit semicircles $s$ with no matching anti-polaric open unit
semicircle in its equivalence class.
Notice $R'$ remain a covering set. When following the steps for Lemma 2 while taking $s$ as the first open unit semicircle to be chosen, the lack of a matching anti-polaric pair for $s$ in $R'$ results that there exists a subset $R\subset R'$ such that $R$ is a covering set and $|R|=3$.
\end{proof}


\begin{observation}
  Minimal covering sets of $\SOtwo$ can be divided into four classes. Looking back at lemmas 1-4, we can see that there are three deviations in the process of selecting a minimal covering set. The first deviation in the process is whether to select the anti-polaric hemisphere of the first hemisphere to be chosen, after selecting the anti-polaric hemisphere and reducing the problem to the 2D parallel, the second deviation is obtained, whether to select the anti-polaric semicircle of the first semicircle to be chosen (which corresponds to the third hemisphere to be chosen in the 3D problem). These two deviations gives us the first two classes if minimal covering sets, one consists of 6 hemishperes and one consists of 5 hemishperes. After getting back to the first deviation and choosing the second option, not to include the anti-polaric hemisphere of the first hemisphere, the last deviation is obtained, whether there are 3 hemisphere that their union cover the rest of $\SOtwo$ and or 4 hemispheres must be taken in order to cover the rest of $\SOtwo$, notice that from lemma 3 we derive that the intersection of these four hemishpere with the boundary of the first hemisphere are 4 semicircles, anti-polaric in pairs.
These two deviations gives us the last two classes, one consists of 4 hemishperes and the last class consists of 5 hemishperes.
\end{observation}

\begin{lemma}
Let $P$ be an input polyhdeon, if there exists a fixture $G$ for $P$ such that its corresponding  covering set belongs in one of the first three classes, then there exists a fixture $G'$ for $P$ such that $G'$ has at most three fingers.
\end{lemma}

\begin{proof}
The proof is of the exhaustive type, lets consider each of the first three classes of covering sets and observe how a three fingers fixture can be made for them.
\begin{enumerate}
\item The first class has 6 hemispheres, anti-polaric in pairs, that involve in the covering set. As seen in Theorem 1, the first hemisphere taken corresponds to the palm facet and another two hemispheres (one of them is the anti-polaric of the first hemisphere), are obtained by the first finger. Three hemispheres are left in the covering set. Lets consider the generation of the second finger, a neighbour the palm facet yields the body facet and a corresponding hemisphere, which leaves us with two hemipsheres needed in order to obtain the covering set, this same body facet has a neighbour that its corresponding hemisphere is not one of the three hemispheres taken into account previously. The reason for the last statement is that this facet has at list three distinct neighbours (a tringle), if the three hemispheres corresponding to these neighbours are exactly the three hemispheres already taken into account, then they involve two anti-polaric hemispheres therefore two of these facet are parallel and hence this facet has at list four neighbours and not three (it cannot be a triangle). In both cases described above, one of the neighbours of this body facet yields a new hemisphere to the set, if the hemisphere is one of two hemispheres left in order to obtain a covering set then only one hemisphere is left in order two obtain this covering set, this additional hemisphere can be obtained with the third finger finishing our proof. The other case where this hemisphere is not one of the two hemispheres left does not bother us because it is out of the scope of this case where there are exactly 6 hemispheres in the covering set, anti-polaric in pairs. Another approach that could make it easier to understand this case is to observe and be convinced that when the class of the covering set of a fixture is the 6 hemispheres class, the geometric embedding of the parts of the workpiece that intersect with the fixture is a parallelepiped, a degenerate case is shown in the floating figure in Section 2.1.

\item The second class has 5 hemispheres, one of them is the anti-polaric hemisphere of the palm corresponding hemisphere. The same claim from Theorem 1 holds in the case too, so the first finger to be generated contributes two hemisphere to the set. Only two hemisphere are left two complete the desired covering set, but those two hemispheres can be obtained by additional two fingers, this sums up to overall three fingers fixture.

\item The third class has 4 hemispheres, one of them hemisphere of the palm facet and the other three can be obtained by three different fingers, therefore this case is finished.

 \end{enumerate}
\end{proof}

\begin{corollary}
Let $P$ be an input polyhderon that holds that there exists no fixture $G$ for $P$ with less then four fingers.
There exists an algorithm that decides for $P$, if there exists a fixture $G$ for $P$ with 4 fingers and outputs the
fixture. The algorithm runs at $O(n^{3})$ time complexity.
\end{corollary}

\begin{algorithm}[htb]
  \caption{4 fingers---fixture generation $O(n^{3})$}
  \label{alg:fixture gen 4 fingers}
  \begin{algorithmic}[1]
    \Require A simple polyhedron $P$
    \Ensure A fixture $G$ that contains four fingers if such one
      exists and if there is no fixture for $P$ with two or three
      fingers. negative output if there is not such fixture.
    \State facetToFingersMapping $\leftarrow$ GetAllPossibleFingerForEveryFacet(P)
    \ForAll{facet $f_{i}$}
      \If {FourCheckValidity(facetToFingersMapping{[}i{]},$f_{i}$) == true}
        \State best $\leftarrow$ FindBest(NULL, facetToFingersMapping{[}i{]})
        \State return best
      \EndIf
    \EndFor
    \State return false
  \end{algorithmic}
\end{algorithm}
\begin{func}{FourCheckValidity}
The key principle of this reduced time complexity check, is to use the fact derived from Lemma 7 that if there are no three or two fixtures to be found for a polyhedron, only fixtures of the last covering set class should be considered. In the last class of covering sets, after choosing the first hemisphere that leave behind a closed hemisphere $C$ of $SOtwo$ to be covered, the second and the third hemispheres to be chosen cover the remaining closed hemisphere except two anti-polaric points. Notice that in this case, although these hemisphere are not anti-polaric, their intersections with $\partial{C}$,which are unit opem semicircles, are anti-polaric. Another observation needed in the algorithm is that there exists a canonical total order on sets of hemispheres that has the same intersections with $\partial{C}$. If two hemisphere $h_1$ and $h_2$ has the same intersections with $\partial{C}$ then $(h_1\cap C) \subset (h_2\cap C)$ or $(h_2\cap C) \subset (h_1\cap C)$ or $h_1 = h_2$. The meaning of this observation to the covering set problem is that if $(h_1\cap C) \subset (h_2\cap C)$ (aka $h_1 <_C h_2$), then $h_2$ can replace $h_1$ in the covering set (after choosing the first hemisphere and obtaining $C$), in all cases. The algorithm goes as follows, a mapping is obtained between the hemispheres corresponding to the facets of the candidate fingers of $f_i$ to their semicircle intersections with $\partial{C}$. Each semicircle remembers only the higher hemisphere according to the ordering described above, this process takes $O(n)$ in time complexity. The next step is iterating  over all the semicircles checking whether their anti-polaric semicircle exists in the mapping, and if so, if the union of the two hemispheres that correspond cover all of $C$ except two points. This check is quite simple, they cover all of $C$ do if and only if the angle between their normal is less then 90 degrees. We know that the hemisphere where taken from an originally covering set, so the two points left can be covered and found in another $O(n)$ time complexity (additive), but all the algorithms until now gave us all the fixtures and stoping here limits us to find only one fixture so the algorithm will continue and find all hemispheres that covers these two points, thus adding another $O(n)$ factor to the algorithm (multiplicative). The overall time complixity of the checking is $O(n)$.
\end{func}

% -----------------------------------------------------------------------------
\subsection{Complixity}
\label{ssec:algorithms:complixity2}
% -----------------------------------------------------------------------------

\begin{enumerate}
\item FourCheckValidity: $O(n^{3})$ The
  function is called once per facet in the input, $O(n)$ times. The time complexity of the checking algorithm is $O(n^2)$, so the overall time complexity is $O(n^3)$.
\end{enumerate}

\begin{corollary}
There exists an algorithm $A$ that for a given polyhedron $P$ decides if
there exists a matching fixture $G$ and outputs the fixture with minimum amount of fingers, iterating over all the possible fixtures of this minimum amount of fingers. $A$ runs at $O(n^{3})$
time.
\end{corollary}
\begin{proof}
Run algorithm 1 activated only on subgroups of fingers of size 3 and 2, if succeed return its output and if fails return output of algorithm 2.
The correctness of Algorithm 1 and 2 where proved previously and the total running time is $O(n^{3}+n^{3}) = O(n^{3})$.
\end{proof}
