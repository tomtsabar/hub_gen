% =============================================================================
\section{Bounds and Properties}
\label{sec:properties}
% =============================================================================
In this section we provide some theoretical bounds and properties,
including the introduction of our main theorem. We use these bounds
and properties to analyze our algorithms and prove their
correctness. We also use one known theorem and several known lemmas,
which we nevertheless explicitly spell out for clarity.

% -----------------------------------------------------------------------------
\subsection{Covering Set}
\label{ssec:properties:covering}
% -----------------------------------------------------------------------------
\begin{definition}[Covering set]
  Let $S=\{s_1,...,s_{|S|}\}$ be a finite set and $C$ be a set of
  points in $\Rd$. If $\bigcup_{i=1}^{|S|} s_{i} \supseteq C$ then $S$
  is a covering set of $C$.
\end{definition}

\begin{theorem*}{Helly's theorem~\cite{hv-httgt-17}}
Let $S=\{ X_{1},...,X_{n}\} $ be a finite set of convex elements that
are subsets of $\mathbb{R}^{d}$. If $\cap_{j=0}^{n}X_{j}=\phi$ then
there exists a subset $R=\{ X_{i_{1}},...,X_{i_{d+1}}\}\subseteq S$
such that $|R|=d+1$ and $\cap_{j=0}^{d+1}X_{i_{j}}=\phi$.
\end{theorem*}

\begin{remark}%Regarding our usage of Helly's theorem.
In the following proofs we use a different formulation of Helly's
theorem; it follows: Let $S=\{ X_{1},...,X_{n}\} $ be a finite set of
convex elements that are subsets of $\mathbb{R}^{d}$. If
$\cup_{j=0}^{n}X_{j}=\mathbb{R}^{d}$ then there exists a subset $R=\{
X_{i_{1}},...,X_{i_{d+1}}\}\subseteq S$ such that $|R|=d+1$ and
$\cup_{j=0}^{d+1}X_{i_{j}}=\mathbb{R}^{d}$. This formulation is
equivalent because the intersection of a set of subgroups of
$\mathbb{R}^{d}$ is empty if and only if the union of their
complement in $\mathbb{R}^{d}$ is $\mathbb{R}^{d}$.
%It derives from Helly's theorem that if the union of convex subsets
%of $\mathbb{R}^{d}$ is $\mathbb{R^{d}}$ exists a subgroup of them
%in size $d+1$ which their union is $\mathbb{R}^{d}$,and that is
%the way we will use it.
\end{remark}

The following four lemmas,
\ref{lemmaA}--\ref{lemmaD},~\cite{shb-spspm-17} are used below to
prove Theorem~\ref{theorem:upper-bound}.

\begin{lemma}\label{lemmaA}
Let $S$ be a finite set of open unit semicircles. If $S$ is a covering
set of a closed unit semicircle $\closedCS$, then there exists
$R\subseteq S$ such that $R$ is a covering set of $\closedCS$ and
$|R|\in\{2,3\}$.
\end{lemma}

\begin{proof}
  It is obvious that one open unit semicircle cannot cover a closed
  unit semicircle. Let $\openCS$ denote the interior of $\closedCS$.
  ($\openCS$ is an open unit semicircle.) There are two cases:
%
  (i) $\openCS \in S$ and $\openCS \in R$ for every
  covering set $R\subseteq S$ of $\closedCS$. It implies that every
  covering set $R\subseteq S$ must contain two additional open
  semicircles that cover the two boundary points of $\closedCS$,
  respectively. These two semicircles together with $\openCS$
  constitute a covering set of $\closedCS$ of size three.
%
  (ii) There exists a covering set $S' \subseteq S$, where
  $\openCS \notin S'$. Let $S'_{\closedCS}=\{s\cap \closedCS\,|\,s\in
  S'\}$ be the set of intersections of the elements of $S'$ and
  $\closedCS$.
%
  Let $\Pi$ denote the extended central projection that maps the
  closed semicircle $\closedCS$ to the projective line, $\Pi(p) =
  (x,w):\closedCS \rightarrow \PPone$, where the points in \PPone are
  represented in homogeneous coordinates $(x,w)$.
%
  Notice that for every $s\in S'_{\closedCS}$, $s$ covers one of the
  boundary points of $\closedCS$; therefore, $\Pi(s)$ is an open ray
  covering either $(-1,0)$ or $(+1,0)$. $S'_{\closedCS}$ covers
  $\closedCS$; therefore, the set of its images
  $S'_{\Pi}=\{\Pi(s)\,|\,s\in S'_{\closedCS}\}$ covers \PPone. By
  Helly's theorem, there exists a subset $R'_{\Pi} \subseteq S'_{\Pi}$
  of size two that covers \Rone. Thus, the set of preimages of
  $R'_{\Pi}$ covers $\closedCS$.
\end{proof}

\begin{lemma}\label{lemmaB}
  Let $S$ be a finite set of open unit semicircles. If $S$ is a
  covering set of the unit circle $\SOone$, then there exists $R
  \subseteq S$ such that $R$ is a covering set of $\SOone$ and $|R| \in
  \{3,4\}$.
\end{lemma}

\begin{proof}
  Let $s \in S$ be an arbitrary open unit semicircle in $S$. The
  reminaing elements $S \setminus \{s\}$ of $S$ must cover the
  complement $\bar{s}$ of $s$, which is a closed unit semicircle.
  By lemma~\ref{lemmaA}, there exists $R' \subseteq S \setminus \{s\}$
  that covers $\bar{s}$, and $|R'| \in \{2,3\}$. Thus, $R' \cup
  \{s\}$ covers $\SOone$, and $|R' \cup \{s\}| \in \{3,4\}$.
\end{proof}

%% \begin{figure}
%% \centering
%% \subfloat[][]{\label{fig:sc}%
%%     \includegraphics[width=0.1\linewidth]{semi_circle}}\hfil
%% \subfloat[][]{\label{fig:c}%
%%     \includegraphics[width=0.2\linewidth]{circle}}\hfill
%% \caption{closed semicircle, circle (left to right)}
%% \end{figure}

\begin{lemma}\label{lemmaC}
Let $S$ be a finite set of open unit hemispheres. If $S$ is a covering
set of a closed unit hemisphere $\closedHS$, then there exists
$R \subseteq S$, such that $R$ is a covering set of $\closedHS$ and
$|R|\in\{3,4,5\}$.
\end{lemma}

\setlength{\intextsep}{0pt}%
\begin{wrapfigure}[8]{R}{3.8cm}
  \begin{tikzpicture}[node distance=5cm]
    \node (img) {\includegraphics[width=3.8cm]{wedge}};
    \node [](B) at (-1.6, -1.2) {\textcolor{color1!80!black}{$\closedCS_1$}};
    \node [](A) at (-1.6, 0.95) {\textcolor{color2!80!black}{$\closedCS_2$}};
  \end{tikzpicture}
\end{wrapfigure}
% \begin{proof}
\noindent\textit{Proof.}
  Let $\openHS$ denote the interior of $\closedHS$ ($\openHS$ is
  an open unit hemisphere) and $\boundaryHS$ denote the boundary of
  $\closedHS$ ($\boundaryHS$ is a great circle).
  %% Naturally, one open unit hemispheres cannot cover a closed unit
  %% hemisphere.
  Similar to the proof of Lemma~\ref{lemmaA}, there are two cases:
%
  (i) $\openHS \in S$ and $\openHS \in R$ for every covering
  set $R \subseteq S$ of $\closedHS$. It implies that every covering set
  $R \subseteq S$ must contain additional open hemispheres that cover
  $\boundaryHS$.
%
  Let $S_{\boundaryHS}=\{s\cap \boundaryHS\,|\,s\in S\}$ be
  the set of intersections of the elements of $S$ and
  $\boundaryHS$. Note that an intersection of a unit open
  hemisphere and a great circle is either empty or an open unit
  semicircle. Therefore $S_{\boundaryHS}$ is a set of open unit
  semicircles lying on the same plane. By Lemma~\ref{lemmaB}, there
  exists a covering set $R_{\boundaryHS} \subset
  S_{\boundaryHS}$ of $\boundaryHS = \SOone$, such that
  $|R_{\boundaryHS}\,|\,\in \{3,4\}$.  It imiplies that there
  exists a covering set $R \subseteq S$ of $\closedHS$, such that $|R| \in
  \{4,5\}$.
%
  (ii) There exists a covering set $S' \subset S$, where $\openHS \notin S'$.
  Let $S'_{\closedHS}=\{s\cap \closedHS\,|\,s\in S'\}$ be the set of
  intersections of the elements of $S'$ and $\closedHS$.
%
  Let $\Pi$ denote the extended central projection that maps the closed
  hemisphere $\closedHS$ to the projective plane,
  $\Pi(p) = (x,y,w):\closedHS \rightarrow \PPtwo$, where the points in
  \PPtwo are represented in homogeneous coordinates $(x,y,w)$.
%
  Notice that every $s\in S'_{\closedHS}$ is a semi-open spherical
  wedge; see the figure above and to the right. The wedge is bounded
  by two semicircles $\closedCS_1$ and $\closedCS_2$ (in the figure),
  where $\closedCS_1$ lies in $\boundaryHS$. The intersection of
  $\closedCS_2$ and $s$ is empty, and the intersection of
  $\closedCS_1$ and $s$ is an open semicircle; therefore, $\Pi(s)$ is
  an open halfplane. $S'_{\closedHS}$ covers $\closedHS$; therefore,
  the set of its images $S'_{\Pi}=\{\Pi(s)\,|\,s\in S'_{\closedHS}\}$
  covers \PPtwo. By Helly's theorem, there exists a minimal subset
  $R'_{\Pi} \subseteq S'_{\Pi}$ of size at most three that covers
  \Rtwo.  If $|R'_{\Pi}|=2$, that is, two open halfplanes, say $h_1$
  and $h_2$ comprise $R'_{\Pi}$, then they must be parallel: $h_1: a x
  + b y + c_1 > 0$ and $h_2: a x + b y + c_2 > 0$.  In this case they
  do not cover the points $(-b, a)$ and $(b, -a)$ in $\PPtwo$.  Thus,
  the pair of preimages of $R'_{\Pi}$ covers $\closedHS$ except for
  two antipolar points. Covering these antipolar points requires two
  additional elements from $S'_{\closedHS}$, which yields a covering
  set of size four. If $|R'_{\Pi}| = 3$, then non of the halfplanes in
  $R'_{\Pi}$ (which cover $\Rtwo$) are parallel, and they also cover
  $\PPtwo$.  Thus, the set of preimages of $ R'_{\Pi}$ covers
  $\closedHS$, which yields a covering set of size three.%
% \end{proof}
\setlength\intextsep{\intextsepSaved}
  %% Let $\Pi$ denote the extended central projection that maps the
  %% closed hemisphere$\closedHS$ to the projective plane, $\Pi(p) =
  %% (x,y,w):\closedHS \rightarrow \PPtwo$, where the points in \PPtwo
  %% are represented in homogeneous coordinates $(x,y,w)$.
  %%
  %% The second case is there exists two hemispheres $h_{1},h_{2}\in S$,
  %% such that the union of $h_{1}$ and $h_{2}$ covers $int(c)$. Let
  %% $\partial{h_{1}}$ and $\partial{h_{2}}$ be the intersection of
  %% $h_{1}$ and $h_{2}$ with the boundary of $C$, $\partial{h_{1}}$ and
  %% $\partial{h_{2}}$ are anti-polaric open unit semicircles. Only two
  %% hemisphere does not intersect with $\partial{C}$, $C$ and the
  %% anti-polaric hemisphere of $C$, $h_{1}$ and $h_{2}$ are not $C$
  %% because $C$ already taken out of $S$ and not anti-polaric to $C$
  %% because this case was covered in case 1, therefore $\partial{h_{1}}$
  %% and $\partial{h_{2}}$ are open unit semicircles. $Pr(h_{1})$ and
  %% $Pr(h_{2})$ are half-planes, the only case where two half-planes
  %% covers $\mathbb{R}^{2}$ is where they are parallel, therefore
  %% $Pr(h_{1})$ and $Pr(h_{2})$ are parallel and $h_{1}$ and $h_{2}$ are
  %% anti-polaric. Because $h_{1}$ and $h_{2}$ are anti-polaric, it
  %% remains to cover two points on $\partial{C}$, each of them is
  %% coverable by one hemisphere of $S$, denote $h_3$ and $h_4$
  %% accordingly. With $R=\{h_1, h_2, h_3, h_4\}$ this case is closed
  %% with a covering set $|R|=4$.
  %%
  %% The third case is where there exists three hemispheres
  %% $h_{1},h_{2},h_{3} \in S$, such that the union of $h_{1}$, $h_{2}$
  %% and $h_{3}$ covers $int(c)$. There always exists such hemispheres,
  %% $P(S)$ is a set of half-planes in $\mathbb{R}^{2}$ that holds
  %% $\cup_{p\in P(S))} = \mathbb{R}^{2}$ because $S$ is a covering set
  %% of $C$. From helly's theorem there exists a subset $Pr(R)\subseteq
  %% P(S)$ such that $|Pr(R)| = 3$ and $P(R)$ holds $\cup_{p\in Pr(R))} =
  %% \mathbb{R}^{2}$, therefore $R = h_{1},h_{2},h_{3}$ is a covering set
  %% of $C$. Let $\partial{h_{1}},\partial{h_{2}}$ and $\partial{h_{3}}$
  %% be the intersection of $h_{1}, h_{2}$ and $h_{3}$ with the boundary
  %% of $C$, $\partial{h_{1}},\partial{h_{2}}$ and $\partial{h_{3}}$ are
  %% open unit semicircles. \todo{$<<<$As seen in Lemmas 1,2 the only
  %%   case where 4 open unit semicircles are needed to cover the unit
  %%   circle is where two of them are anti-polaric, this option was
  %%   covered in case 2. $Pr(h_{1}), Pr(h_{2}), Pr(h_{3})$ covers all
  %%   directions of $\mathbb{R}^{2}$ therefore
  %%   $\partial{h_{1}},\partial{h_{2}}$ and $\partial{h_{3}}$ covers all
  %%   direction of $\partial{C}$ and form a covering set$>>>$}. With
  %% $R=\{h_1, h_2, h_3\}$ this case is closed with a covering set
  %% $|R|=3$.

\begin{lemma}\label{lemmaD}
  Let $S$ be a finite set of open unit hemispheres. If $S$ is a
  covering set of a unit sphere $\SOtwo$, then there exists
  $R\subseteq S$ such that $R$ is a covering set of $\SOtwo$ and
  $|R|\in\{4,5,6\}$.
\end{lemma}

\begin{proof}
  Let $s \in S$ be an arbitrary open unit hemisphere in $S$. The
  reminaing elements $S \setminus \{s\}$ of $S$ must cover the
  complement $\bar{s}$ of $s$, which is a closed unit hemisphere.
  By lemma~\ref{lemmaC}, there exists $R' \subseteq S \setminus \{s\}$
  that covers $\bar{s}$, and $|R'| \in \{3,4,5\}$. Thus, $R' \cup
  \{s\}$ covers $\SOtwo$, and $|R' \cup \{s\}| \in \{4,5,6\}$.
\end{proof}

%% \begin{figure}
%% \centering
%% \subfloat[][]{\label{fig:closed_hs}%
%%     \includegraphics[width=0.3\linewidth]{closed_hemisphere}}\hfil
%% \subfloat[][]{\label{fig:open_hs}%
%%     \includegraphics[width=0.3\linewidth]{open_hemisphere}}\hfill
%% \caption{(a) open hemisphere, (b) closed hemisphere(left to right)}
%% \end{figure}

\begin{corollary}\label{corollary:parallelepiped}
  Let $R$ be a set of six open unit hemispheres that cover the unit
  sphere $\SOtwo$. If $R$ is minimal, then it must consist of three
  pairs of open unit hemisphere, such that the closure of every pair
  is the entire unit sphere.
\end{corollary}

\begin{proof}
  Assume, by contradiction that $R$ contains an open unit hemispheres
  $h$, such that the interior of its complement is not in $R$. Observe
  that the complement of $h$ is equivalent to $\HOtwo$. This is exactly
  case (ii) in the proof of Lemma~\ref{lemmaC}. Here, there exists a
  covering set $R'$ of $\HOtwo$, such that $|R'| \in \{3,4\}$. It
  implies that $|R|$ is at most five, a contradiction.
\end{proof}

When a facet $f$ of the workpiece coincides with a facet of the
fixture, the workpiece cannot translate in any direction that forms an
acute angle with the (outer) normal to the plane containg $f$ (without
colliding with the fixture). This set of blocking directions comprises
an open unit hemisphere denoted as $h(f)$. Similarly, $\calH(\calF) =
\{h(f)\,|\,f \in \calF\}$ denote the mapping from a set of facets to the
set of corresponding open unit hemispheres; see, e.g.,
\cite{shb-spspm-17}. Let $\calF'$ denote the set of facets of the
workpiece that are coincident with facets of the fixture in some
configuration. If the union of all blocking directions comprise the
unit sphere, formally stated $\SOtwo = \bigcup \calH(\calF')$, then the
workpiece cannot translate at all.

Let $\calF$ denote the set of all facets of $G$. Let $\calF_{P}$
denote the singleton that consists of the base facet of the palm of
$G$, and let $f_{b_{i}}$ and $f_{g_{i}}$, $1\leq i\leq k$, denote the
base facet of the body and the base facet of the gripper,
respectively, of the $i$-th finger of $G$, where $k$ indicates the
number of fingers. Let $\calF_{B}=\{f_{b_{i}}\,|\,1\leq i\leq k\}$ and
$\calF_{G}=\{f_{g_{i}}\,|\,1\leq i\leq k\}$ denote the set of the base
facets of the body parts of the fingers of $G$ and the set of the base
facets of the gripper parts of the fingers of $G$, respectively. Let
$\calF_{PBG}$ denote the set of all base facets of the parts of $G$,
that is $\calF_{PBG} = \calF_{P} \cup \calF_{B} \cup \calF_{G}$.  Let
$\calF_{PB}$ denote the set of all base facets of the parts of $G$
excluding the base facets of the grippers, that is,
$\calF_{PB} = \calF_{P} \cup {\calF}_{B}$.

\begin{corollary}\label{corollary:proper-fixture}
  For a proper fixture $G$ the following must hold:
  \begin{enumerate}
  \item $\SOtwo=\bigcup \calH(\calF_{PBG})$.
  \item $\SOtwo\neq\bigcup \calH(\calF_{PB})$.
  \end{enumerate}
\end{corollary}

A candidate finger of an input polyhedron $P$ is a valid finger of at
least one possible fixture $G$ of $P$.

\begin{observation}\label{observation:candidate-fingers}
  The number of candidate fingers of an input polyhedron $P$ is linear
  in the number of vertices.
\end{observation}

\begin{proof}
  Let $e$ be an edge of $P$ and let $f_{e}$ and $f_{e}'$ be the two
  faces incident to $e$. Two fingers can be built on $e$. The base facet
  of the body and the base facet of the gripper of one coincides with
  $f_{e}$ and $f_{e}'$, respectively. In the other finger the roles of
  these facets exchange; that is, the base facet of the body and the
  base facet of the gripper coincides with $f_{e}'$ and $f_{e}$,
  respectively. Every candidate finger is built on a single edge. Thus,
  the number of candidate finger is at most $2|E|$. By Euler's formula
  the number of edges in a polyhedron is at most $3n-6$, where $n$ is
  the number of vertices of the polyhedron. Thus, the number of
  candidate fingers is at most $6n-12$.
\end{proof}

\begin{theorem}\label{theorem:upper-bound}
  The minimal number of fingers of all proper fixtures of any given polyhedron
  is four. This bound is tight.
\end{theorem}

\begin{proof}
 %% Consider a polyhedron $P$. Let $G$ be a proper fixture if $P$. By
  %%Corollary~\ref{corollary:proper-fixture}, $\SOtwo=\bigcup
  %%\calH(\calF_{PBG})$. %% By Lemma~\ref{lemmaD} there exists a subset
 %% $R \subset \calH(\calF_{PBG})$, such that $|R| \in\{4,5,6\}$.

%%  If $|R| = 6$, by Corollary~\ref{corollary:parallelepiped}, $R$
%%  consists of three pairs of open unit hemisphere, such that the
%%  closure of every pair is the entire unit sphere. It implies that $P$
%%  has three pairs of parallel facets---a parallelepiped. Regardless of
%%  $G$, if $P$ is a parallelepiped, then $P$ has six distinct proper
%% fixtures that have four fingers each. The procedure to generate one
%% follows. Pick an arbitrary facet $f_p$ and use it as the base facet
%% of the palm of a fixture. Then, use each one of the four neighbour
%% facets of $f_p$ as the base facet of a body of a distinct
%%  finger. Finally, use the last facet of $P$ (which is parallel to
%%  $f_p$) as the base facet of each of the grippers of the four
%% fingers. This establishes the upper bound for this case.

%%  It remains to prove the upper bound for the case where $|R|\in\{4,5\}$.
%%  \tom{Hey Efi, the following proof holds all cases and maybe the paragraph %%above is redundant.}

Consider a polyhedron $P$. Let $G$ be a proper fixture of $P$. By
Corollary~\ref{corollary:proper-fixture},
$\SOtwo=\bigcup\calH(\calF_{PBG})$.  Let $G$ be a proper fixture $G$
for $P$ such that $G$ composed of more than four fingers. In the proof
$G'$ will be constructed by taking the palm facet of $G$ and a subset
of fingers of $G$ such that $G'$ will be composed of at most four
fingers. Let $p_G$ be the palm facet of $G$, after excluding the
hemisphere that corresponds to $p_G$ from the covering set, a closed
unit hemisphere is left to be covered by the hemispheres corresponding
to the fingers of $G'$. $ \SOtwo=\bigcup \calH(\calF_{PBG})$ and hence
$\closedHS = \SOtwo\setminus \calH(\calF_{P})\subseteq\bigcup
\calH(\calF_{BG})$. As seen \ref{lemmaC} if there exists a covering
set for a closed unit hemisphere there exist a subset $R$ of this
covering set that holds (1) $R$ is a covering set (2) $|R|\in
\{3,4,5\}$.  The first case is simple, there exists a subset $R$ of $
\calH(\calF_{BG})$ such that $|R|\in \{3,4\}$ and $R$ is a covering
set of $\closedHS$. These three or four hemispheres corresponds to
three or four facets who belongs to at most three or four fingers of
$G$, $G'$ will be constructed from the palm of $G$ and those fingers,
the last steps promise that $G'$ is a proper fixture, becuase a
covering set is formed by the facets of $G'$, and because $G'$ is
composed of palm and fingers of another proper fixture it holds
$\SOtwo\neq\bigcup \calH(\calF_{P'B'})$.

The second case requires additional step, there exists a subset $R$ of
$ \calH(\calF_{BG})$ such that $|R|=5$ and $R$ is a covering set of
$\closedHS$. As seen in Lemma~\ref{lemmaC} the case $|R|=5$ only
occurs when $\openHS\in R$.  The facet who corresponds to this open
unit hemisphere must be a parallel of $p_G$. In a polyhedron, two
parallel faces can not be neighbours, thus this facet must be a
gripper facet of some finger.  Following case 1 in Lemma~\ref{lemmaC}
it remains to cover the unit circle, after including the body's open
unit hemisphere of the same finger, which is added without the cost of
adding an additional finger, it remains to cover a closed unit
semicircle, that is because the body's open unit hemisphere
intersection with $\boundaryHS$ is a open semisircle. As seen in
Lemma~\ref{lemmaA}, three open unit semicircles, which match the
intersections of three open unit hemispheres with $\boundaryHS$, are
suffice to cover $\closedCS$. The three open unit hemispheres
corresponds to at most three additional fingers of $G$, therefore a
fixture $G'$ can be constructed from the palm of $G$ and four other
fingers of $G$. $G'$ is composed of palm and fingers of another proper
fixture therefore it holds $\SOtwo\neq\bigcup \calH(\calF_{P'B'})$. In
conclusion, the minimal number of fingers of all proper fixtures of
any given polyhedron is four.

In order to show the bound is tight, an example of such a polyhedron
must be provided, Figure~\ref{fig:special} and an off file included to
this paper describe such a polyhedron. Proving that there exists no
fixture with less then four fingers for this polyhedron is exhaustive
and done by iterating and checking all possible configurations with
the $O(n^5)$ algorithm described in the next section.

\end{proof}
\begin{figure}[ht]
	\centering
	\subfloat[][]{\label{fig:special:1}%
		\includegraphics[width=0.2\linewidth]{s_1}}\hfil
	\subfloat[][]{\label{fig:special:2}%
		\includegraphics[width=0.2\linewidth]{s_2}}\hfil
	\subfloat[][]{\label{fig:special:3}%
		\includegraphics[width=0.2\linewidth]{s_3}}\hfil
	\caption[]{Different views of the specialhedron, a polyhedron that have one valid fixture with 4 fingers}
	\label{fig:special}
\end{figure}
%% \begin{proof}
%% Let $G$ be a fixture of $P$. If $G$ contains less then 5 fingers then
%% $G'=G$. If the number of fingers of $G$ is 5 or more we will observe
%% at $H(B_{G})$, the set of open unit hemispheres corresponds to the
%% facets of $G$ that interact with $P$. The palm contributes a single
%% open unit hemisphere to the covering set. As seen in
%% Lemma~\ref{lemmaC} and 4 after choosing the first open unit
%% hemisphere, choosing additional 3, 4 or 5 open unit hemispheres is
%% enough in order to form a covering set. In the first case where 3 or 4
%% additional open unit hemispheres are sufficient, these 3 or 4 open
%% unit hemispheres corresponds to at most 4 fingers, a fixture $G'$
%% build from these 4 fingers assembled on the palm generates the desired
%% fixture. That is because $H(B_{G'})$ form a covering set, $H(B_{G'L})$
%% is a subset of $H(B_{GL})$ therefore it does not form a covering set,
%% and all of $G'$ fingers were fingers of $G$ therefore they are
%% valid. The last case is the case where 5 additional open unit
%% hemispheres are needed to form a covering set. As seem in
%% Lemma~\ref{lemmaC} and~\ref{lemmaD} this case is the case where the
%% open unit hemisphere who is anti-polar to the open unit hemisphere
%% corresponding to palm must be included in the covering set. Therefore
%% the geometric embedding of the facet corresponding to this open unit
%% hemisphere must be parallel to the plane of palm geometric embedding
%% . A simple fact is that two facets that their geometric embeddings are
%% parallel can not be neighbours in a polyhedron thus the other facet
%% must be a gripper facet. Following case 1 in Lemma~\ref{lemmaC} it
%% remains to cover the unit circle, after including the body open unit
%% hemisphere of the same finger, which can be added without the cost of
%% an additional finger, it remains to cover a closed unit
%% semicircle. That is because the body open unit hemisphere intersection
%% with the unit circle is a open unit semicircle. As seen in
%% Lemma~\ref{lemmaA}, only 3 additional open unit semicircles (that
%% corresponding to 3 additional open unit hemispheres), are needed to
%% cover the rest. These 3 open unit hemispheres corresponds to at most 3
%% fingers, a fixture $G'$ build from the first finger in addition to
%% these 3 fingers assembled on the palm generates the desired
%% fixture. That is because $H(B_{G'})$ form a covering set, $H(B_{G'L})$
%% is a subset of $H(B_{GL})$ therefore it does not form a covering set,
%% and all of $G'$ fingers were fingers of $G$ therefore they are
%% valid. Both cases ends up generating $G'$ with at most 4 fingers.
%% \end{proof}

\begin{observation}
  A fixture with only one finger does not exist.
\end{observation}

\begin{proof}
Only two open unit hemispheres corresponds to a single finger,
including the $palm$'s open unit hemisphere it sums up to 3 open unit
hemispheres, as seen in Lemma~\ref{lemmaD} the minimum size of a
covering set of $\SOtwo$ is 4.
\end{proof}

\begin{example}
The lower bound is tight. There exists a input polyhedron $P$ and a
fixture $G$ of $P$ such that $G$ has only two fingers (see
Figure~\ref{fig:fixture}, and off file included to this paper).
\end{example}
