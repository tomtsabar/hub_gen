project(flex_fixture)
cmake_minimum_required(VERSION 2.8.11)

# Use C++11 for this directory and its sub-directories.
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

# CGAL and its components
if (NOT CGAL_FOUND)
  find_package(CGAL REQUIRED COMPONENTS)
  if (NOT CGAL_FOUND)
    message(STATUS "This project cannot be compiled, as it requires CGAL.")
    return()
  endif()
endif()

# include helper file
include( ${CGAL_USE_FILE})

# Boost and its components
find_package(Boost REQUIRED)

if (NOT Boost_FOUND)
  message(STATUS "This project requires the Boost library, and will not be compiled.")
  return()
endif()

find_package(SGAL REQUIRED)

# include for local directory

# include for local package

include_directories(${Boost_INCLUDE_DIR})
include_directories(${SGAL_INCLUDE_DIRS})
include_directories(../../libs/SFG/include)
add_executable(flex_fixture_old flex_fixture_old.cpp)
target_link_libraries(flex_fixture_old ${Boost_LIBRARIES})
