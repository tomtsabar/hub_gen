// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University.
//
// This file is private property of Tel Aviv University.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>
//            Tom Tsabar        <tomtsabar9@gmail.com>

#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <unordered_set>

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/number_utils.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/Kernel/global_functions.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
#include <CGAL/IO/Polyhedron_builder_from_STL.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/bbox.h>

#include "SGAL/basic.hpp"
#include "SGAL/compute_planes.hpp"
#include "SGAL/Epic_kernel.hpp"
#include "SGAL/Epic_polyhedron.hpp"

#include "SFG/plane_projector.hpp"
#include "SFG/check_upper_facet.hpp"
#include "SFG/find_covering_set.hpp"
#include "SFG/general_utils.hpp"
#include "SFG/hub_utils.hpp"
#include "SFG/merge_approximate_coplanar_facets.hpp"

namespace pmp = CGAL::Polygon_mesh_processing;

#define DEFAULT_FIXTURE_WIDTH 0.08
#define DEFAULT_FINGER_WIDTH 0.07
#define DEFAULT_GRIPPER_PENETRATION 0.05
#define DEFAULT_GRIPPER_EXTRA 0.2
#define MAX_ARM 4
#define MIN_ARM 2

typedef SGAL::Epic_kernel                               Kernel;
typedef SGAL::Epic_polyhedron                           Polyhedron;

typedef Kernel::Direction_3                             Direction_3;
typedef Polyhedron::Point_3                             Point_3;
typedef Kernel::Vector_3                                Vector_3;

typedef Polyhedron::Halfedge_handle                     Halfedge_handle;
typedef Polyhedron::Facet_handle                        Facet_handle;
typedef Polyhedron::Facet_const_handle                  Facet_const_handle;
typedef Polyhedron::Vertex_handle                       Vertex_handle;
typedef Kernel::Plane_3                                 Plane;

typedef CGAL::Surface_mesh<Kernel::Point_3>             Surface_mesh;

typedef std::list<Direction_3>                          Direction_patch;


typedef std::pair<std::vector<Polyhedron::Facet_const_iterator>, Direction_3> Top_facet;
typedef boost::is_same<Polyhedron::Plane_3, Plane>          Polyhedron_has_plane;


static Polyhedron* s_polyhedron(nullptr);

/*
 * thanks for Hugo Ledoux for MergeCoplanarFacets*/


/*
 * Prints all the options to use the code.
 */
void printUsage()
{
  std::cerr << "Usage:" << std::endl;
  std::cerr << "./gen_hub <input.off> (uses default)" << std::endl;
  std::cerr
    << "./gen_hub <input.off> [-p5] [-palm] [-f fixture_width] [-a arm_width] [-g gripper_penetration.off] [-e gripper extra]"
    << std::endl;
  std::cerr << "The default equal to:" << std::endl;
  std::cerr << "./gen_hub <input.off> -f 0.08 -a 0.07 -g 0.05 -e 0.2"
            << std::endl;
}

/*
 * Frees memory before exiting the program.
 */
int freeMemAndFinish(
                     std::vector<std::pair<FacetUtil, std::vector<Finger>*>> mapFacetToFingers)
{
  int i;
  for (i = 0; i < mapFacetToFingers.size(); i++)
  {
    delete mapFacetToFingers[i].second;
  }
  return 0;
}

/*
 * Parses the arguments
 */
int parse_args(int argc, char* argv[], float* fixture_width, float* arm_width,
               float* gripper_pen, float* gripper_extra, int* enforce_property, int* force_palm_facet,
               char** filename)
{

  *fixture_width = DEFAULT_FIXTURE_WIDTH;
  *arm_width = DEFAULT_FINGER_WIDTH;
  *gripper_pen = DEFAULT_GRIPPER_PENETRATION;
  *gripper_extra = DEFAULT_GRIPPER_EXTRA;
  *force_palm_facet = -1;

  if (argc > 1)
  {
    *filename = argv[1];
  }
  else
  {
    printUsage();
    return -1;
  }

  if (cmdOptionExists(argv, argv + argc, "-f"))
  {
    *fixture_width = atof(getCmdOption(argv, argv + argc, "-f"));
  }

  if (cmdOptionExists(argv, argv + argc, "-a"))
  {
    *arm_width = atof(getCmdOption(argv, argv + argc, "-a"));
  }

  if (cmdOptionExists(argv, argv + argc, "-g"))
  {
    *gripper_pen = atof(getCmdOption(argv, argv + argc, "-g"));
  }

  if (cmdOptionExists(argv, argv + argc, "-e"))
  {
    *gripper_extra = atof(getCmdOption(argv, argv + argc, "-e"));
  }

  if (cmdOptionExists(argv, argv + argc, "-palm"))
  {
    *force_palm_facet = atoi(getCmdOption(argv, argv + argc, "-palm"));
  }

  //Activate enforcement of property 5
  if (cmdOptionExists(argv, argv + argc, "-p5"))
  {
    *enforce_property = 1;
  }
  else
  {
    *enforce_property = 0;
  }

  return 0;
}

//Return parameters generated name
std::string genName(int fingers, int i, int j, std::string add = "" )
{
  std::string name("out/out");
  std::string end = ".off";
  name += std::to_string(fingers);
  name += "-";
  name += std::to_string(i);
  name += "-";
  name += std::to_string(j);
  name += add;
  name += end;
  return name;
}

//
void createOutDir()
{
  std::string sPath = "out";
  mode_t nMode = 0733;
  int nError = 0;

  //Used on Windows
#if defined(_WIN32)
  _mkdir(sPath.c_str());

  //Used on non-Windows
#else
  mkdir(sPath.c_str(), nMode);
#endif

}

inline bool ends_with(std::string const & value, std::string const & ending)
{
  if (ending.size() > value.size())
  {
    return false;

  }
  return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

bool read_stl(const char* filename, Polyhedron& polyhedron)
{
  std::ifstream in(filename, std::ios::in | std::ios::binary);
  if (!in) {
    std::cerr << "Error! Cannot open file " << filename << std::endl;
    return false;
  }

  std::vector<std::array<double, 3> > points;
  std::vector<std::array<int, 3> > triangles;
  if (!CGAL::read_STL(in, points, triangles)) {
    std::cerr << "Error: invalid STL file" << std::endl;
    return false;
  }

  try {
    // Try building a surface_mesh
    if (pmp::is_polygon_soup_a_polygon_mesh(triangles))
      pmp::polygon_soup_to_polygon_mesh(points, triangles, polyhedron);
    if (! polyhedron.is_valid() || polyhedron.is_empty()) {
      std::cerr << "Error: Invalid facegraph" << std::endl;
      return false;
    }
  }
  catch(...){}

  return true;
}

bool read3DFile(char* filename, Polyhedron &poly)
{
  //Check file type
  if (ends_with(filename, ".off"))
  {
    //Transform the off file into a polyhedron
    std::ifstream input(filename);
    if (!input || !(input >> poly) || poly.empty())
    {
      std::cerr << filename << " is not a valid off file." << std::endl;
      return false;
    }
  }
  else
  {
    if (ends_with(filename, ".stl"))
    {
      if (read_stl(filename,poly)==false)
      {
        return false;
      }
    }
    else
    {
      std::cerr << filename << " is not a valid 3D file." << std::endl;
      return false;
    }
  }


  Kernel kernel;

  std::cerr << " before merge" << std::endl;
  SFG::merge_approximate_coplanar_facets(kernel, poly, boost::false_type());

  return true;
}

int main(int argc, char* argv[])
{

  Polyhedron poly;
  int i, j, k, m, fingers;
  FacetUtil base;
  FacetUtil fingers_base[4];
  FacetUtil fingers_grippers[4];

  typename CGAL::Exact_predicates_exact_constructions_kernel::Plane_3 pp;
  typename CGAL::Exact_predicates_exact_constructions_kernel::Line_2 ppp;

  std::vector<std::pair<FacetUtil, std::vector<Finger>*>> mapFacetToFingers;
  std::vector<std::vector<int>*> subgroups;

  std::vector<Halfedge_handle> edges;
  std::vector<Finger> setOffFingers;
  float fixture_width;
  float arm_width;
  float gripper_pen;
  float gripper_extra;
  int enforce_property5;
  int force_palm_facet = -1;
  char* filename;

  int last_finger;

  std::string name;

  //Get the arguments from the user and set the default if the correct arguments cannot be found.
  if (parse_args(argc, argv, &fixture_width, &arm_width, &gripper_pen,
                 &gripper_extra, &enforce_property5, &force_palm_facet, &filename) < 0)
  {
    return -1;
  }

  //Creates the directory out if doesn't exists
  createOutDir();

  //Read 3D file into a polyhedron class
  if (read3DFile(filename, poly) == false)
  {
    return -1;
  }

  //Gets the max size of P for width of hub calculations
  //max_size = get_max_size(poly);

  //Get the mapping of facets (possible palms) to their possible fingers
  getPossibleFingersForEveryPalm(poly, &mapFacetToFingers, enforce_property5);

  std::cerr <<"polyhedron with" << mapFacetToFingers.size() << " facets." << std::endl;
  /*Checks for every configuration possible if this configuration forms a valid fixture.
   * If it is valid fixture, 3D model the fixture.
   * The algorithm:
   *   1. Iterate over all possible amount of fingers
   *   a.  Iterate over all possible palms
   *   1) Iterate over all possible subgroup of fingers corresponds the palm, in the size selected in 1.
   *   a) Check if the configuration obtained forms a valid fixture.
   */
  for (fingers = MIN_ARM; fingers <= MAX_ARM; fingers++)
  {
    //Iterate over all possible palms
    last_finger = mapFacetToFingers.size();
    i = 0;


    //Itirate only over one facet as palm facet
    if (force_palm_facet != -1 && force_palm_facet < mapFacetToFingers.size())
    {
      i = force_palm_facet;
      last_finger = force_palm_facet+1;
    }

    //std::cout << last_finger << ":"<< i
    //  << " fingers" << std::endl;
    for (; i < last_finger; i++)
    {
      subgroups.clear();
      getSubgroups(fingers, mapFacetToFingers[i].second->size(),
                   subgroups);

      //std::cout << i << " : " << normalized(mapFacetToFingers[i].first.plane.orthogonal_vector()) << std::endl;


      for (j = 0; j < subgroups.size(); j++)
      {
      	std::string add_name("-");
        setOffFingers.clear();
        for (k = 0; k < fingers; k++)
        {
            setOffFingers.push_back(
                                  mapFacetToFingers[i].second->at(
                                                                  subgroups[j]->at(k)));
            add_name += std::to_string(subgroups[j]->at(k));
            add_name +="I";
        }

        //Checking validity of a configuration.
        // The process includes:
        // Checking the fixture forms a covering set.
        // Checking the fixture without the grippers doesn't forms a covering set.
        // Additional user defined checks in aproveSet function.
        //
        if (checkFingersAndBase(mapFacetToFingers[i].first.plane,
                                setOffFingers) && aproveSet(setOffFingers))
        {

          //Converts to configuration to facets for the generation process.
          for (m = 0; m < fingers; m++)
          {
            Finger ar = setOffFingers[m];
            fingers_base[m] = ar.body;
            fingers_grippers[m] = ar.gripper;
          }

          //Generate name.
          name = genName(fingers, i, j, add_name);

          //Generating .off file of the hub
          CGAL::Surface_mesh<Kernel::Point_3> fixture_mesh;
          build_hub_from(poly, mapFacetToFingers[i].first,
                         fingers_base, fingers_grippers, fingers,
                         fixture_width, arm_width, gripper_pen,
                         gripper_extra, fixture_mesh);
          //Saves the mesh to a file.
          std::ofstream os(name.c_str());
          os << fixture_mesh;

          name.clear();
          //std::cout << "Success found hub with: " << fingers
          //<< " fingers" << std::endl;
          //std::cout << i << ":"<< last_finger
          //<< " fingers" << std::endl;

        }
      }
    }
  }

  freeMemAndFinish(mapFacetToFingers);
  delSubGroup(subgroups);
  return 0;

}
