// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University.
//
// This file is private property of Tel Aviv University.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>
//            Tom Tsabar        <tomtsabar9@gmail.com>

#ifndef SFG_BASIC_HPP
#define SFG_BASIC_HPP

#include "SGAL/basic.hpp"
#include "SFG/config.hpp"

#endif
