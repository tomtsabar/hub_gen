// Copyright (c) 2008 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>
//            Tom Tsabar         <tomtsabar9@gmail.com>

#ifndef SFG_APPROXIMATE_MERGE_COPLANAR_FACETS_HPP
#define SFG_APPROXIMATE_MERGE_COPLANAR_FACETS_HPP

/*! \file
 * This file contains a few function that accepts a polyhedron. It merges
 * cooplanar facets.
 */

#include <list>
#include <boost/type_traits.hpp>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <unordered_set>

#include <CGAL/number_utils.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/Kernel/global_functions.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
#include <CGAL/Surface_mesh.h>

#include "SGAL/basic.hpp"
#include "SGAL/Epic_kernel.hpp"
#include "SGAL/Epic_polyhedron.hpp"

#include "SFG/basic.hpp"
#include "SFG/hub_utils.hpp"

SFG_BEGIN_NAMESPACE

//If the angle between to facets is smaller then MERGE_EPSILON when they get merged
#define SFG_MERGE_EPSILON 0.000000001

typedef SGAL::Epic_kernel               Kernel;
typedef SGAL::Epic_polyhedron           Polyhedron;

/*! Merge coplanar facets */
template<typename Polyhedron, typename Equal>
void merge_approximate_coplanar_facets_impl(Polyhedron& polyhedron, Equal& eq)
{
  typedef typename Polyhedron::Halfedge_handle Halfedge_handle;
  typedef std::list<Halfedge_handle> Edge_container;

  // Insert all edges incident to coplanar faces to the coplanar edge container
  Edge_container edges;
  for (auto eit = polyhedron.edges_begin(); eit != polyhedron.edges_end();
       ++eit)
  {
    auto ohe = eit->opposite();
    // Bail out if the polyhedron is invalid
    if (eit == 0 || ohe == 0 || eit->facet() == 0 || ohe->facet() == 0) {
      return;
    }

    if (eq(plane(eit->facet()).orthogonal_vector(),
           plane(ohe->facet()).orthogonal_vector()))
    {
      edges.push_back(eit);
    }
  }

  // Traverse cooplanar edges and remove them.
  for (auto it = edges.begin(); it != edges.end(); ++it) {
    auto he = *it;
    auto ohe = he->opposite();

    if (he->facet() == ohe->facet()) continue;
    if ((he->vertex()->degree() > 2) && (ohe->vertex()->degree() > 2))
      polyhedron.join_facet(he);
    else if (he->vertex()->degree() == 2) polyhedron.join_vertex(ohe);
    else polyhedron.join_vertex(he);
  }

  // Traverse all vertices and remove vertices of degree 2.
  for (auto vit = polyhedron.vertices_begin();
       vit != polyhedron.vertices_end(); ++vit)
  {
    if (vit->degree() == 2) {
      auto he = vit->vertex_begin();
      auto ohe = he->opposite();
      polyhedron.join_vertex(ohe);
    }
  }
}

template <typename Kernel>
struct My_equal {
  Kernel& m_kernel;
  float m_epsilon;

  My_equal(Kernel& kernel, float epsilon) :
    m_kernel(kernel),
    m_epsilon(epsilon)
  { }

  template<typename Vector>
  bool operator()(const Vector& v1, const Vector& v2)
  {
    typename Kernel::Equal_3 eq = m_kernel.equal_3_object();
    if (normalized(v1) * normalized(v2) >= (1 - m_epsilon)) return 1;
    return eq(v1.direction(), v2.direction());
  }
};

template <typename Kernel, typename Polyhedron>
void merge_approximate_coplanar_facets(Kernel& kernel, Polyhedron& polyhedron,
                                       float epsilon = SFG_MERGE_EPSILON)
{
  My_equal<Kernel> eq(kernel, epsilon);
  if (polyhedron.size_of_border_edges() > 0)
    std::cout << "Waring: The polyhedron has holes!" << std::endl;
  merge_approximate_coplanar_facets_impl(polyhedron, eq);
}

SFG_END_NAMESPACE

#endif
