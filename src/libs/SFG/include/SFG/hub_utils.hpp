// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University.
//
// This file is private property of Tel Aviv University.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>
//            Tom Tsabar        <tomtsabar9@gmail.com>

#include <fstream>
#include <string>
#include <vector>
#include <list>
#include <unordered_set>
#include <algorithm>

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/number_utils.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/IO/Polyhedron_iostream.h>
#include <CGAL/boost/graph/graph_traits_Polyhedron_3.h>
#include <CGAL/Kernel/global_functions.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
//#include "../../include/CGAL/Set_movable_separability_3/lp_wrapper.h"
#include <CGAL/Surface_mesh.h>

#include "SGAL/basic.hpp"
#include "SGAL/Epic_kernel.hpp"
#include "SGAL/Epic_polyhedron.hpp"

#include "SFG/plane_projector.hpp"
#include "SFG/check_upper_facet.hpp"
#include "SFG/find_covering_set.hpp"

#ifndef SFG_HUB_UTILS_HPP
#define SFG_HUB_UTILS_HPP

typedef SGAL::Epic_kernel                               Kernel;
typedef SGAL::Epic_polyhedron                           Polyhedron;

typedef Kernel::Direction_3                             Direction_3;
typedef Polyhedron::Point_3                             Point_3;
typedef Kernel::Vector_3                                Vector_3;

typedef Polyhedron::Halfedge_handle                     Halfedge_handle;
typedef Polyhedron::Facet_handle                        Facet_handle;
typedef Polyhedron::Facet_const_handle                  Facet_const_handle;
typedef Polyhedron::Vertex_handle                       Vertex_handle;
typedef Kernel::Plane_3                                 Plane;

typedef CGAL::Surface_mesh<Kernel::Point_3>             Surface_mesh;

typedef std::list<Direction_3>                          Direction_patch;

typedef std::pair<std::vector<Polyhedron::Facet_const_iterator>, Direction_3>
  Top_facet;

//Return the plane equation corresponds to a facet.
inline Plane plane(Facet_handle f)
{
  Halfedge_handle h = f->halfedge();

  return Plane(h->vertex()->point(), h->next()->vertex()->point(),
               h->next()->next()->vertex()->point());
}

//Holds data about where new facet connect to the fixture.
struct Facet_connector {
  Surface_mesh::Vertex_index first;
  Surface_mesh::Vertex_index second;
  Halfedge_handle edge;
};

//Holds index of a facet and its plane equation.
struct FacetUtil {
  int index;
  Plane plane;
};

//Holds data of two points.
struct Dpoint {
  Point_3 a;
  Point_3 b;
};

//Holds the data needed to construct a finger.
struct Finger {
  FacetUtil body;
  FacetUtil gripper;
};

//Holds the data of a surface, holds both its 3D points and their indices in the surface mesh.
struct DualSurface {
  std::vector<Point_3> points;
  std::vector<Surface_mesh::Vertex_index> indices;

};

//Holds all the connection points of the fingers.
struct ConnectionPoints {
  Point_3 low[4];
  Point_3 high[4];
};

//Multiple a point with a scalar (multiplication as a vector).
inline Point_3 mul_point(Point_3 p, double scalar)
{
  Vector_3 vp = p - CGAL::ORIGIN;
  vp *= scalar;
  return CGAL::ORIGIN + vp;
}

//Add two 3D points.
inline Point_3 add_points(Point_3 p, Point_3 q)
{
  Vector_3 vp = p - CGAL::ORIGIN;
  Vector_3 vq = q - CGAL::ORIGIN;

  return CGAL::ORIGIN + (vp + vq);
}

//Normalize a 3D vector
inline Vector_3 normalized(Vector_3 v)
{
  return v / sqrt(v.squared_length());
}

//Get the maximum size of a polyhedron P (just the maximum between its hight, width and depth nothing complicated).
inline float get_max_size(Polyhedron p)
{
  Point_3 temp;
  float xmax, xmin, ymax, ymin, zmax, zmin;
  for (auto vit = p.vertices_begin(); vit != p.vertices_end(); ++vit) {
    if (vit == p.vertices_begin()) {
      temp = vit->point();
      xmax = CGAL::to_double(temp.x());
      xmin = CGAL::to_double(temp.x());
      ymax = CGAL::to_double(temp.y());
      ymin = CGAL::to_double(temp.y());
      zmax = CGAL::to_double(temp.z());
      zmin = CGAL::to_double(temp.z());
    }
    else {
      temp = vit->point();
      if (CGAL::to_double(temp.x()) > xmax) xmax = CGAL::to_double(temp.x());
      if (CGAL::to_double(temp.x()) < xmin) xmin = CGAL::to_double(temp.x());

      if (CGAL::to_double(temp.y()) > ymax) ymax = CGAL::to_double(temp.y());
      if (CGAL::to_double(temp.y()) < ymin) ymin = CGAL::to_double(temp.y());

      if (CGAL::to_double(temp.z()) > zmax) zmax = CGAL::to_double(temp.z());
      if (CGAL::to_double(temp.z()) < zmin) zmin = CGAL::to_double(temp.z());
    }
  }
  return max3(xmax - xmin, ymax - ymin, zmax - zmin);
}

//Checks if set of directions forms a covering set.
inline bool is_covering_set(const std::vector<Direction_3> dirs)
{

  int outLength;
  int i;
  unsigned int outIndexs[6];
  Direction_3 outDirection;
  bool outDirectionExists;
  std::vector<typename Polyhedron::Facet_const_iterator> null_fill;

  std::vector<
    std::pair<typename Kernel::Direction_3,
              std::vector<typename Polyhedron::Facet_const_iterator>>> polyhedronNormals;
  for (i = 0; i < dirs.size(); i++) {
    polyhedronNormals.push_back(std::make_pair(dirs[i], null_fill));
  }
  outLength = findCoveringSet<Kernel>(polyhedronNormals, outIndexs);
  if (outLength != NOFOUND && outLength != 0) return true;
  return false;

}

//Checks if a fixture forms a covering set.
//Checks if a fixture without the gripper forms a covering set.
//If both criteria are true return that the fixture is valid.
inline bool checkFingersAndBase(Plane base, std::vector<Finger> fingers)
{
  std::vector<Direction_3> dirs;
  int i = 0;
  dirs.push_back(base.orthogonal_direction());
  for (i = 0; i < fingers.size(); i++) {
    dirs.push_back(fingers[i].body.plane.orthogonal_direction());

  }
  if (is_covering_set(dirs)) {
    return false;
  }
  for (i = 0; i < fingers.size(); i++) {
    dirs.push_back(fingers[i].gripper.plane.orthogonal_direction());
  }
  if (is_covering_set(dirs)) return true;
  return false;
}

//Add indices to the polygon facets.
inline Facet_handle poly_insert_indices(Polyhedron& p, FacetUtil base)
{
  int i = 0;
  Facet_handle hBase;
  for (auto it = p.facets_begin(); it != p.facets_end(); ++it) {
    it->id() = i;
    if (i == base.index) hBase = it;
    ++i;
  }
  return hBase;
}

//Creating palm extrusion for finger with sharp angle between palm and body
inline void
gen_base_sharp_finger(ConnectionPoints* base_inner_conn, int i,
                      Facet_handle palm,
                      Vector_3* arm_extrude_vectors,
                      DualSurface* base_inner_surface,
                      Facet_connector* base_inner_connecter,
                      Facet_connector* base_outer_connecter,
                      float arm_width,
                      Surface_mesh* mesh,
                      Surface_mesh::Vertex_index* arm_base_con_vertices_low,
                      Surface_mesh::Vertex_index* arm_base_con_vertices_high)
{
  Surface_mesh::Vertex_index arm_base_con_vertices_low_float;
  Surface_mesh::Vertex_index arm_base_con_vertices_high_float;

  Vector_3 edge = base_inner_conn->high[i] - base_inner_conn->low[i];
  Vector_3 arm_offset =
    CGAL::cross_product(plane(palm).orthogonal_vector(), edge);
  arm_extrude_vectors[i] = normalized(arm_offset) * (arm_width);
  arm_base_con_vertices_low_float =
    mesh->add_vertex(base_inner_conn->low[i] - arm_extrude_vectors[i]);
  arm_base_con_vertices_high_float =
    mesh->add_vertex(base_inner_conn->high[i] - arm_extrude_vectors[i]);

  base_inner_surface->indices.push_back(arm_base_con_vertices_high[i]);
  base_inner_connecter[i].first = arm_base_con_vertices_low[i];
  base_inner_connecter[i].second = arm_base_con_vertices_high[i];

  base_outer_connecter[i].first = arm_base_con_vertices_low_float;
  base_outer_connecter[i].second = arm_base_con_vertices_high_float;
  base_inner_surface->points.push_back(base_inner_conn->low[i]);
  base_inner_surface->points.push_back(base_inner_conn->high[i]);
}

//Creating palm extrusion for finger with blunt angle between palm and body
inline void
gen_base_blunt_finger(ConnectionPoints* base_inner_conn, int i,
                      Facet_handle palm, Vector_3* arm_extrude_vectors,
                      DualSurface* base_inner_surface,
                      Facet_connector* base_inner_connecter,
                      float arm_width, Surface_mesh* mesh,
                      Surface_mesh::Vertex_index* arm_base_con_vertices_low,
                      Surface_mesh::Vertex_index* arm_base_con_vertices_high)
{

  Surface_mesh::Vertex_index arm_base_con_vertices_low_float;
  Surface_mesh::Vertex_index arm_base_con_vertices_high_float;

  Vector_3 edge = base_inner_conn->high[i] - base_inner_conn->low[i];
  Vector_3 arm_offset = CGAL::cross_product(plane(palm).orthogonal_vector(),
                                            edge);
  arm_extrude_vectors[i] = normalized(arm_offset) * (arm_width);

  /*arm_base_con_vertices_low_float = mesh->add_vertex(
    base_inner_conn->low[i]);
    arm_base_con_vertices_high_float = mesh->add_vertex(
    base_inner_conn->high[i]);*/

  base_inner_surface->indices.push_back(arm_base_con_vertices_high[i]);

  base_inner_connecter[i].first = arm_base_con_vertices_low[i];
  base_inner_connecter[i].second = arm_base_con_vertices_high[i];

  //base_outer_connecter[i].first = arm_base_con_vertices_low_float;
  //base_outer_connecter[i].second = arm_base_con_vertices_high_float;

  base_inner_surface->points.push_back(base_inner_conn->low[i]);
  base_inner_surface->points.push_back(base_inner_conn->high[i]);
}

//Iterating over the edges of the palm facet to get its vertices.
//Creates the new vertices needed to connect the fingers to the palm.
//Generates the palm inner facet.
inline void
gen_base_inner_facet_and_connectors(Facet_handle base_handle, int size,
                                    FacetUtil* arms_base,
                                    FacetUtil* arms_grippers, float arm_width,
                                    Surface_mesh* mesh,
                                    DualSurface* base_inner_surface,
                                    Facet_connector* base_inner_connecter,
                                    Facet_connector* base_outer_connecter,
                                    ConnectionPoints* base_inner_conn,
                                    Vector_3* arm_extrude_vectors,
                                    bool* sharp)
{
  Surface_mesh::Vertex_index arm_base_con_vertices_low[4];
  Surface_mesh::Vertex_index arm_base_con_vertices_high[4];

  Halfedge_handle circ;
  Halfedge_handle circ2;
  circ = base_handle->halfedge();
  Facet_handle palm = circ->facet();

  Point_3 gripper_end;
  Vector_3 gripper_direction;
  int i;
  do {
    for (i = 0; i < size; ++i) {
      if (circ->opposite()->facet() != 0
          && circ->opposite()->facet()->id() == arms_base[i].index)
      {

        base_inner_conn->low[i] =
          add_points(mul_point(circ->vertex()->point(), 0.3),
                     mul_point(circ->prev()->vertex()->point(), 0.7));
        base_inner_conn->high[i] =
          add_points(mul_point(circ->vertex()->point(), 0.7),
                     mul_point(circ->prev()->vertex()->point(), 0.3));

        arm_base_con_vertices_low[i] = mesh->add_vertex(base_inner_conn->low[i]);
        arm_base_con_vertices_high[i] = mesh->add_vertex(base_inner_conn->high[i]);

        base_inner_surface->indices.push_back(arm_base_con_vertices_low[i]);

        //check if finger is sharp

        base_inner_connecter[i].edge = circ->opposite();

        circ2 = base_inner_connecter[i].edge;
        do {
          if (circ2->opposite()->facet() != 0 &&
              circ2->opposite()->facet()->id() == arms_grippers[i].index)
          {

            gripper_end = add_points(mul_point(circ2->vertex()->point(), 0.3),
                                     mul_point(circ2->prev()->vertex()->point(),
                                               0.7));
            break;
          }

          circ2 = circ2->next();

        } while (circ2 != base_inner_connecter[i].edge);

        gripper_direction = gripper_end - base_inner_conn->low[i];
        //Finish check if finger is sharp
        if (plane(palm).orthogonal_vector() * gripper_direction < 0) {
          sharp[i] = true;
          gen_base_sharp_finger(base_inner_conn, i, palm,
                                arm_extrude_vectors, base_inner_surface,
                                base_inner_connecter, base_outer_connecter,
                                arm_width, mesh, arm_base_con_vertices_low,
                                arm_base_con_vertices_high);

        }
        else {
          sharp[i] = false;
          gen_base_blunt_finger(base_inner_conn, i, palm,
                                arm_extrude_vectors, base_inner_surface,
                                base_inner_connecter, arm_width, mesh,
                                arm_base_con_vertices_low,
                                arm_base_con_vertices_high);
        }

      }
    }

    Surface_mesh::Vertex_index temp = mesh->add_vertex(
                                                       circ->vertex()->point());
    base_inner_surface->points.push_back(circ->vertex()->point());
    base_inner_surface->indices.push_back(temp);
    circ = circ->next();
  } while (circ != base_handle->halfedge());

}

//Generate the palm outer facet and side facets.
//Outputs additional data needed to connect the fingers.
inline void
gen_base_out_facets_and_sides(int size, DualSurface base_inner_surface,
                              Facet_connector base_inner_connecter[4],
                              Facet_connector base_outer_connecter[4],
                              ConnectionPoints base_inner_conn_points,
                              Vector_3 arm_extrude_vectors[4], FacetUtil base,
                              float hub_width,
                              float arm_width, Surface_mesh* mesh,
                              DualSurface extrudes_surfaces[4],
                              bool* sharp)
{
  std::vector<Surface_mesh::Vertex_index> base_sides_f;
  std::vector<Surface_mesh::Vertex_index> base_sides_b;
  std::vector<Surface_mesh::Vertex_index> base_outer;
  std::vector<Surface_mesh::Vertex_index> base_outer_add;
  std::vector<Surface_mesh::Vertex_index> base_inner;
  std::vector<Surface_mesh::Vertex_index> reverse_face;

  Vector_3 arm_exstrudes[4];

  std::vector<Surface_mesh::Vertex_index> replace;
  int arm_index = -1;
  int i, j, k;
  base_outer.clear();
  base_outer_add.clear();

  for (i = 0; i < base_inner_surface.indices.size(); i++)
  {
    base_inner.push_back(base_inner_surface.indices[i]);

    if (i == 0) k = 1;
    else if (i == base_inner_surface.indices.size() - 1) k = 2;
    else k = 0;
    Surface_mesh::Vertex_index temp =
      mesh->add_vertex(base_inner_surface.points[i] +
                       normalized(base.plane.orthogonal_vector()) * (hub_width));

    for (j = 0; j < size; j++) {
      if (base_inner_surface.points[i] == base_inner_conn_points.high[j]) {
        k = 1;
        arm_index = j;
        Facet_handle f1 =
          base_inner_connecter[j].edge->opposite()->facet();
        Vector_3 edge = base_inner_conn_points.high[j]
          - base_inner_conn_points.low[j];
        Vector_3 arm_offset = CGAL::cross_product(plane(f1).orthogonal_vector(), edge);
        arm_extrude_vectors[j] = normalized(arm_offset) * (arm_width);

        //Add topology that is needed in order to generate blunt fingers
        if (!sharp[j])
        {
          arm_extrude_vectors[j] *= -1;
        }

        extrudes_surfaces[j].points.push_back(base_inner_conn_points.low[j]
                                              + normalized(base.plane.orthogonal_vector())
                                              * (hub_width) - arm_extrude_vectors[j]);
        extrudes_surfaces[j].points.push_back(base_inner_conn_points.high[j]
                                              + normalized(base.plane.orthogonal_vector())
                                              * (hub_width) - arm_extrude_vectors[j]);

        extrudes_surfaces[j].indices.push_back(base_outer[i - 1]);

        extrudes_surfaces[j].indices.push_back(mesh->add_vertex(extrudes_surfaces[j].points[0]));

        extrudes_surfaces[j].indices.push_back(mesh->add_vertex(extrudes_surfaces[j].points[1]));

        extrudes_surfaces[j].indices.push_back(temp);

        base_outer_add.push_back(extrudes_surfaces[j].indices[1]);
        base_outer_add.push_back(extrudes_surfaces[j].indices[2]);

      }
    }

    //The switch is about connecting side facets of the base
    switch (k) {
      //regular
     case 0:

      base_sides_f.push_back(base_inner_surface.indices[i]);
      base_sides_f.push_back(temp);
      //Add side facet
      mesh->add_face(base_sides_f);
      base_sides_f.clear();
      base_sides_b.clear();

      base_sides_b.push_back(temp);
      base_sides_b.push_back(base_inner_surface.indices[i]);

      replace = base_sides_b;
      base_sides_b = base_sides_f;
      base_sides_f = replace;
      break;
      //start or jump around an arm
     case 1:
      //base_sides_f.clear();
      if (arm_index != -1) {
        if (sharp[arm_index]) {
          base_sides_f.push_back(base_outer_connecter[arm_index].first);
          base_sides_f.push_back(extrudes_surfaces[arm_index].indices[1]);

          mesh->add_face(base_sides_f);
        }

        base_sides_f.clear();

        if (sharp[arm_index]) {
          base_sides_f.push_back(extrudes_surfaces[arm_index].indices[1]);
          base_sides_f.push_back(base_outer_connecter[arm_index].first);
          base_sides_f.push_back(base_outer_connecter[arm_index].second);
          base_sides_f.push_back(extrudes_surfaces[arm_index].indices[2]);

          mesh->add_face(base_sides_f);

          base_sides_f.clear();
        }
        base_sides_f.push_back(extrudes_surfaces[arm_index].indices[2]);
        if (sharp[arm_index]) {
          base_sides_f.push_back(
                                 base_outer_connecter[arm_index].second);
          base_sides_f.push_back(base_inner_surface.indices[i]);
          base_sides_f.push_back(temp);

          mesh->add_face(base_sides_f);
        }

        extrudes_surfaces[arm_index].indices[0] =
          base_inner_surface.indices[i - 1];
        if (sharp[arm_index]) {
          extrudes_surfaces[arm_index].indices[1] =
            base_outer_connecter[arm_index].first;
          extrudes_surfaces[arm_index].indices[2] =
            base_outer_connecter[arm_index].second;
        }
        extrudes_surfaces[arm_index].indices[3] =
          base_inner_surface.indices[i];
        //arm_ext_vertices[arm_index]

      }
      base_sides_f.clear();
      base_sides_b.clear();

      base_sides_b.push_back(temp);
      base_sides_b.push_back(base_inner_surface.indices[i]);

      replace = base_sides_b;
      base_sides_b = base_sides_f;
      base_sides_f = replace;

      break;
      //last

     case 2:

      base_sides_f.push_back(base_inner_surface.indices[i]);
      base_sides_f.push_back(temp);

      //Add side facet
      mesh->add_face(base_sides_f);
      base_sides_f.clear();
      base_sides_b.clear();

      base_sides_b.push_back(temp);
      base_sides_b.push_back(base_inner_surface.indices[i]);

      base_sides_b.push_back(base_inner_surface.indices[0]);
      base_sides_b.push_back(base_outer[0]);

      //Add side facet
      mesh->add_face(base_sides_b);
      break;
    }

    base_outer.push_back(temp);
    base_outer_add.push_back(temp);
  }

  //Add inner base facet
  for (i = base_inner.size() - 1; i >= 0; i--) {
    reverse_face.push_back(base_inner[i]);
  }
  mesh->add_face(reverse_face);

  //Add outer base facet
  mesh->add_face(base_outer_add);
}

//Generates the body of the sharp fingers.
//Outputs additional data needed to connect the body of the fingers to their gripper.
inline void
gen_fingers_body_sharp(float gripper_extra, FacetUtil arms_grippers,
                       Facet_connector base_inner_connecter,
                       DualSurface extrudes_surfaces,
                       Vector_3 arm_extrude_vectors, Surface_mesh* mesh,
                       std::vector<Surface_mesh::Vertex_index>& arm_vertices,
                       std::vector<Surface_mesh::Vertex_index>& arm_parallel_vertices,
                       DualSurface& gripper_inner_surface,
                       Facet_connector& gripper_connecter,
                       Plane& side_planes)
{
  std::vector<Surface_mesh::Vertex_index> new_face;
  Halfedge_handle circ;
  int j;

  Point_3 low_middle;
  Point_3 high_middle;
  circ = base_inner_connecter.edge;

  do {
    if (circ->opposite()->facet() != 0 &&
        circ->opposite()->facet()->id() == arms_grippers.index)
    {
      low_middle = add_points(mul_point(circ->vertex()->point(), 0.3),
                              mul_point(circ->prev()->vertex()->point(), 0.7));
      high_middle = add_points(mul_point(circ->vertex()->point(), 0.7),
                               mul_point(circ->prev()->vertex()->point(), 0.3));

      gripper_inner_surface.points.push_back(low_middle);
      gripper_inner_surface.points.push_back(high_middle);

      Surface_mesh::Vertex_index low = mesh->add_vertex(low_middle);
      Surface_mesh::Vertex_index high = mesh->add_vertex(high_middle);
      arm_vertices.push_back(low);
      arm_vertices.push_back(high);

      gripper_connecter.edge = circ->opposite();
      gripper_connecter.first = low;
      gripper_connecter.second = high;

    }

    circ = circ->next();

  } while (circ != base_inner_connecter.edge);

  arm_vertices.push_back(base_inner_connecter.second);
  arm_vertices.push_back(base_inner_connecter.first);

  new_face.clear();
  for (j = arm_vertices.size() - 1; j >= 0; j--) {
    new_face.push_back(arm_vertices[j]);
  }

  //Adding the arm inner facaet
  mesh->add_face(new_face);

  //Adding them arm parallel facet
  Point_3 l_before_extra = low_middle - arm_extrude_vectors;
  Point_3 h_before_extra = high_middle - arm_extrude_vectors;

  Vector_3 l_extra = l_before_extra
    - extrudes_surfaces.points[0]/*arm_joint_low[i]*/;
  Vector_3 h_extra = h_before_extra
    - extrudes_surfaces.points[1]/*arm_joint_high[i]*/;

  side_planes = Plane(h_before_extra + (normalized(h_extra) * (gripper_extra)),
                      h_before_extra, high_middle);

  //Start check if finger is sharp
  float sharp_finger_extra = gripper_extra;

  circ = gripper_connecter.edge;

  Facet_handle f1 = circ->facet();

  Vector_3 gripper_offset =
    CGAL::cross_product(plane(f1).orthogonal_vector(),
                        -side_planes.orthogonal_vector());

  Facet_handle f2 = circ->opposite()->facet();

  if (plane(f2).orthogonal_vector() * gripper_offset < 0) {
    sharp_finger_extra = 0;
  }

  //Finish check if finger is sharp

  arm_parallel_vertices.push_back(mesh->add_vertex(h_before_extra +
                                                   (normalized(h_extra) *
                                                    (sharp_finger_extra))));

  arm_parallel_vertices.push_back(extrudes_surfaces.indices[2]);
  arm_parallel_vertices.push_back(extrudes_surfaces.indices[1]);

  arm_parallel_vertices.push_back(mesh->add_vertex(l_before_extra +
                                                   (normalized(l_extra) *
                                                    (sharp_finger_extra))));

  mesh->add_face(arm_parallel_vertices);
}

//Generates the body of the blunt fingers.
//Outputs additional data needed to connect the body of the fingers to their gripper.
inline void
gen_fingers_body_blunt(float gripper_extra,
                       FacetUtil arms_grippers,
                       Facet_connector base_inner_connecter,
                       DualSurface extrudes_surfaces,
                       Vector_3 arm_extrude_vectors,
                       Surface_mesh* mesh,
                       std::vector<Surface_mesh::Vertex_index>& arm_vertices,
                       std::vector<Surface_mesh::Vertex_index>& arm_parallel_vertices,
                       DualSurface& gripper_inner_surface,
                       Facet_connector& gripper_connecter,
                       Plane &side_planes)
{

  std::vector<Surface_mesh::Vertex_index> new_face;
  Halfedge_handle circ;
  int j;

  Point_3 low_middle;
  Point_3 high_middle;
  circ = base_inner_connecter.edge;

  do {
    if (circ->opposite()->facet() != 0
        && circ->opposite()->facet()->id() == arms_grippers.index)
    {
      low_middle = add_points(mul_point(circ->vertex()->point(), 0.3),
                              mul_point(circ->prev()->vertex()->point(), 0.7));
      high_middle = add_points(mul_point(circ->vertex()->point(), 0.7),
                               mul_point(circ->prev()->vertex()->point(), 0.3));

      gripper_inner_surface.points.push_back(low_middle);
      gripper_inner_surface.points.push_back(high_middle);

      Surface_mesh::Vertex_index low = mesh->add_vertex(low_middle);
      Surface_mesh::Vertex_index high = mesh->add_vertex(high_middle);
      arm_vertices.push_back(low);
      arm_vertices.push_back(high);

      gripper_connecter.edge = circ->opposite();
      gripper_connecter.first = low;
      gripper_connecter.second = high;

    }

    circ = circ->next();

  } while (circ != base_inner_connecter.edge);

  arm_vertices.push_back(base_inner_connecter.second);
  arm_vertices.push_back(base_inner_connecter.first);
  new_face.clear();
  for (j = arm_vertices.size() - 1; j >= 0; j--) {
    new_face.push_back(arm_vertices[j]);
  }

  //Adding the arm inner facaet
  mesh->add_face(new_face);

  //Adding them arm parallel facet
  Point_3 l_before_extra = low_middle - arm_extrude_vectors;
  Point_3 h_before_extra = high_middle - arm_extrude_vectors;

  Vector_3 l_extra = l_before_extra
    - extrudes_surfaces.points[0]/*arm_joint_low[i]*/;
  Vector_3 h_extra = h_before_extra
    - extrudes_surfaces.points[1]/*arm_joint_high[i]*/;
  side_planes = Plane(h_before_extra + (normalized(h_extra) * (gripper_extra)),
                      h_before_extra, high_middle);

  //Start check if finger is sharp
  float sharp_finger_extra = gripper_extra;

  circ = gripper_connecter.edge;

  Facet_handle f1 = circ->facet();

  Vector_3 gripper_offset = CGAL::cross_product(plane(f1).orthogonal_vector(),
                                                -side_planes.orthogonal_vector());

  Facet_handle f2 = circ->opposite()->facet();

  if (plane(f2).orthogonal_vector() * gripper_offset < 0)
  {
    sharp_finger_extra = 0;
  }

  //Finish check if finger is sharp

  arm_parallel_vertices.push_back(mesh->add_vertex(h_before_extra
                                                   + (normalized(h_extra) * (sharp_finger_extra))));

  arm_parallel_vertices.push_back(extrudes_surfaces.indices[2]);
  arm_parallel_vertices.push_back(extrudes_surfaces.indices[1]);

  arm_parallel_vertices.push_back(mesh->add_vertex(l_before_extra
                                                   + (normalized(l_extra) * (sharp_finger_extra))));
  mesh->add_face(arm_parallel_vertices);
}

//Generates the grippers of the fingers.
inline void
gen_fingers_grippers(float gripper_pen,
                     std::vector<Surface_mesh::Vertex_index> arm_vertices,
                     std::vector<Surface_mesh::Vertex_index> arm_parallel_vertices,
                     Facet_connector gripper_connecter,
                     DualSurface gripper_inner_surface,
                     Plane side_planes, Surface_mesh* mesh)
{
  std::vector<Surface_mesh::Vertex_index> gripper_upper;
  std::vector<Surface_mesh::Vertex_index> gripper_side;
  std::vector<Surface_mesh::Vertex_index> gripper_vertices;

  Halfedge_handle circ;
  //int i;
  //for (i = 0; i < size; i++)

  //{
  gripper_vertices.clear();

  circ = gripper_connecter.edge;

  Facet_handle f1 = circ->facet();
  Vector_3 gripper_offset =
    CGAL::cross_product(plane(f1).orthogonal_vector(),
                        -side_planes.orthogonal_vector());

  Vector_3 gripper_extrude = normalized(gripper_offset) * (gripper_pen);

  Point_3 low_middle = gripper_inner_surface.points[0] - gripper_extrude;

  Point_3 high_middle = gripper_inner_surface.points[1] - gripper_extrude;
  gripper_inner_surface.points.push_back(low_middle);
  gripper_inner_surface.points.push_back(high_middle);

  Surface_mesh::Vertex_index low = mesh->add_vertex(low_middle);
  Surface_mesh::Vertex_index high = mesh->add_vertex(high_middle);

  gripper_vertices.push_back(high);
  gripper_vertices.push_back(low);
  gripper_vertices.push_back(gripper_connecter.first);
  gripper_vertices.push_back(gripper_connecter.second);

  //Add gripper inner
  mesh->add_face(gripper_vertices);

  //Gripper upper facet
  gripper_upper.clear();
  gripper_upper.push_back(high);
  gripper_upper.push_back(arm_parallel_vertices[0]);
  gripper_upper.push_back(arm_parallel_vertices[3]);
  gripper_upper.push_back(low);

  mesh->add_face(gripper_upper);

  //Left side
  gripper_side.clear();
  gripper_side.push_back(arm_parallel_vertices[0]);
  gripper_side.push_back(high);
  gripper_side.push_back(gripper_connecter.second);
  gripper_side.push_back(arm_vertices[2]);
  gripper_side.push_back(arm_parallel_vertices[1]);
  mesh->add_face(gripper_side);

  //Right side
  gripper_side.clear();
  gripper_side.push_back(arm_parallel_vertices[3]);
  gripper_side.push_back(arm_parallel_vertices[2]);
  gripper_side.push_back(arm_vertices[3]);
  gripper_side.push_back(gripper_connecter.first);
  gripper_side.push_back(low);

  mesh->add_face(gripper_side);

  //}
}

//Generate a 3D fixture from the input polyhedron and the choosen fixture configuration.
inline void build_hub_from(Polyhedron p, FacetUtil base,
                           FacetUtil* arms_base, FacetUtil* arms_grippers,
                           int size, float hub_width, float arm_width,
                           float gripper_pen, float gripper_extra,
                           Surface_mesh& mesh)
{
  bool sharp[4];
  int finger_index;

  DualSurface base_inner_surface;
  DualSurface gripper_inner_surface[4];
  DualSurface extrudes_surfaces[4];

  ConnectionPoints base_inner_conn_points;

  Facet_connector base_inner_connecter[4];
  Facet_connector base_outer_connecter[4];
  Facet_connector gripper_connecter[4];

  std::vector<Surface_mesh::Vertex_index> arm_parallel_vertices[4];
  std::vector<Surface_mesh::Vertex_index> arm_vertices[4];
  Vector_3 arm_extrude_vectors[4];

  Vector_3 low_arm_angle[4];
  Vector_3 high_arm_angle[4];

  Plane side_planes[4];

  Facet_handle base_handle;

  //Sets id for each facet TODO understand if necessary
  base_handle = poly_insert_indices(p, base);
  //Generate the inner base facet and output:
  //  1. The data of the outer base facet
  //  2. The data of the fingers that connect to the inner base facet
  gen_base_inner_facet_and_connectors(base_handle, size, arms_base,
                                      arms_grippers, arm_width, &mesh,
                                      &base_inner_surface,
                                      base_inner_connecter,
                                      base_outer_connecter,
                                      &base_inner_conn_points,
                                      arm_extrude_vectors, sharp);
  //Creates the base outer facets while closing its side facets
  //It saves all the arms's places of connection for later use
  gen_base_out_facets_and_sides(size, base_inner_surface,
                                base_inner_connecter, base_outer_connecter,
                                base_inner_conn_points,
                                arm_extrude_vectors, base, hub_width, arm_width,
                                &mesh,
                                extrudes_surfaces, sharp);

  //Creates all fingers
  for (finger_index = 0; finger_index < size; finger_index++) {
    if (sharp[finger_index]) {
      //Adds the arms body's facets again while saving the indices to connect the gripper
      gen_fingers_body_sharp(gripper_extra, arms_grippers[finger_index],
                             base_inner_connecter[finger_index],
                             extrudes_surfaces[finger_index],
                             arm_extrude_vectors[finger_index], &mesh,
                             arm_vertices[finger_index],
                             arm_parallel_vertices[finger_index],
                             gripper_inner_surface[finger_index],
                             gripper_connecter[finger_index],
                             side_planes[finger_index]);
    }
    else {
      //Adds the arms body's facets again while saving the indices to connect the gripper
      gen_fingers_body_blunt(gripper_extra, arms_grippers[finger_index],
                             base_inner_connecter[finger_index],
                             extrudes_surfaces[finger_index],
                             arm_extrude_vectors[finger_index], &mesh,
                             arm_vertices[finger_index],
                             arm_parallel_vertices[finger_index],
                             gripper_inner_surface[finger_index],
                             gripper_connecter[finger_index], side_planes[finger_index]);

    }

    //Now for the grippers.
    gen_fingers_grippers(gripper_pen, arm_vertices[finger_index],
                         arm_parallel_vertices[finger_index],
                         gripper_connecter[finger_index],
                         gripper_inner_surface[finger_index],
                         side_planes[finger_index],
                         &mesh);
  }
  //CGAL::Polygon_mesh_processing::triangulate_faces(mesh);
}

//Enforce property 5 from the paper.
//Property 5 states that gripper facets can't be neighbors of the palm facet.
inline int checkP5(int id, Halfedge_handle f2edge)
{
  Halfedge_handle circ = f2edge;
  do {
    if (circ->opposite()->facet()->id() == id) return 0;

    circ = circ->next();
  } while (circ != f2edge);
  return 1;
}

//Gets all the possible fingers to be stretched from any possible palm.
inline void getPossibleFingersForEveryPalm(Polyhedron p,
                                           std::vector<std::pair<FacetUtil,
                                           std::vector<Finger>*>>* mapping,
                                           int enforceP5)
{
  int i;
  int j;

  i = 0;
  CGAL::set_halfedgeds_items_id(p);
  for (auto it = p.facets_begin(); it != p.facets_end(); it++) {
    FacetUtil tmp;
    tmp.index = it->id();
    tmp.plane = plane(it);
    mapping->push_back(std::make_pair(tmp, new std::vector<Finger>()));

    //mapping[i].second = new std::vector<Arm>();
    i++;
  }

  for (auto eit = p.halfedges_begin(); eit != p.halfedges_end(); ++eit) {
    Facet_handle f1 = eit->facet();
    Facet_handle f2 = eit->opposite()->facet();
    if (f1 == 0 || f2 == 0) continue;
    if (f1 == f2 ) continue;

    //Arm making
    Finger finger12;
    int id;

    id = f1->id();
    finger12.body.index = id;
    finger12.body.plane = plane(f1);

    id = f2->id();
    finger12.gripper.index = id;
    finger12.gripper.plane = plane(f2);

    //Insert arm12 to f1 neighbors
    Halfedge_handle circ;
    circ = f1->halfedge();        //e1->next();
    do {
      if (circ->opposite()->facet() != f2 && circ->opposite()->facet() != f1) {
        if (circ->opposite()->facet() != 0) {
          if (!enforceP5 ||
              checkP5(circ->opposite()->facet()->id(), f2->halfedge()))
          {
            id = circ->opposite()->facet()->id();

            //Check that body and gripper differ from palm facet
            if (circ->opposite()->facet() == f1 or  circ->opposite()->facet() == f2 ) continue;

            mapping->at(id).second->push_back(finger12);
          }
        }
      }

      circ = circ->next();
    } while (circ != f1->halfedge());
  }

}

//Checks that their no 2 body facets who are the same.
//Could be modified in later versions/
inline bool aproveSet(std::vector<Finger> set)
{
  if (set.size() == 2) {
    if (set[0].body.index == set[1].body.index) return false;
  }
  if (set.size() == 3) {
    if (set[0].body.index == set[1].body.index) return false;
    if (set[2].body.index == set[1].body.index) return false;
    if (set[0].body.index == set[2].body.index) return false;
  }
  if (set.size() == 4) {
    if (set[0].body.index == set[1].body.index) return false;
    if (set[0].body.index == set[2].body.index) return false;
    if (set[0].body.index == set[3].body.index) return false;
    if (set[1].body.index == set[2].body.index) return false;
    if (set[1].body.index == set[3].body.index) return false;
    if (set[2].body.index == set[3].body.index) return false;
  }

  return true;
}

#endif
