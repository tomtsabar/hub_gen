// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University.
//
// This file is private property of Tel Aviv University.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#ifndef SFG_CONFIG_HPP
#define SFG_CONFIG_HPP

#include <boost/config.hpp>

// #include "SGAL/version.hpp"

#define SFG_BEGIN_NAMESPACE namespace SFG {
#define SFG_END_NAMESPACE }

///////////////////////////////////////////////////////////////////////////////
// Windows DLL suport
#ifdef BOOST_HAS_DECLSPEC
#if defined(SGAL_ALL_DYN_LINK) || defined(SGAL_SGAL_DYN_LINK)
// export if this is our own source, otherwise import:
#ifdef SGAL_SFG_SOURCE
# define SGAL_SFG_DECL __declspec(dllexport)
#else
# define SGAL_SFG_DECL __declspec(dllimport)
#endif  // SGAL_SFG_SOURCE
#endif  // DYN_LINK
#endif  // BOOST_HAS_DECLSPEC

#ifndef SGAL_SFG_DECL
#define SGAL_SFG_DECL
#endif

#endif
