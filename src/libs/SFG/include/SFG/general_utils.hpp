// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University.
//
// This file is private property of Tel Aviv University.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>
// Author(s)     : Tom Tsabar         <tomtsabar9@gmail.com>

#ifndef SFG_GENERAL_UTILS_HPP
#define SFG_GENERAL_UTILS_HPP

// Gets command argument
inline char* getCmdOption(char** begin, char** end, const std::string& option)
{
  char** itr = std::find(begin, end, option);
  if (itr != end && ++itr != end) {
    return *itr;
  }
  return 0;
}

//Check if user entered a command argument
inline bool cmdOptionExists(char** begin, char** end, const std::string& option)
{ return std::find(begin, end, option) != end; }

inline float max3(float a,float b,float c)
{
  if (a > b && a > c) return a;
  if (b > c) return b;
  return c;
}

//Return n choose k
unsigned long long
inline choose(unsigned long long n, unsigned long long k) {
  if (k > n) {
    return 0;
  }
  unsigned long long r = 1;
  for (unsigned long long d = 1; d <= k; ++d) {
    r *= n--;
    r /= d;
  }
  return r;
}

//Delete subgroup vector from memory
inline void delSubGroup(std::vector<std::vector<int>*>& ret)
{ for (auto i = 0; i < ret.size(); ++i) delete ret[i]; }

//Return all the subgroup size k in [n] in ret
inline void getSubgroups(int k,int n, std::vector<std::vector<int>*>& ret)
{
  if (n < k) return ;

  int coords[4];
  int i,j;
  int steps = choose(n,k);
  bool next = true;

  for(i = 0; i < k; i++) coords[i] = i;
  j = 0;
  while (true) {
    ret.push_back(new std::vector<int>());
    for (i=0;i<k;i++) ret[j]->push_back(coords[i]);
    if (j == steps-1) break;
    next = true;
    i = k -1;
    while (next) {
      if (coords[i] == n-1-(k-1-i)) {
        --i;
        next = true;
      }
      else {
        coords[i] += 1;
        next = false;
      }
    }
    for(;i<k-1;i++) coords[i+1]=coords[i]+1;
    j++;
  }
}

#endif
