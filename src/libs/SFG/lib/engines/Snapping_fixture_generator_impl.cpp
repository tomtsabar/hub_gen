// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University.
//
// This file is private property of Tel Aviv University.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>
//            Tom Tsabar        <tomtsabar9@gmail.com>

#include <vector>
#include <iostream>

#include <CGAL/Surface_mesh.h>

#include "SGAL/basic.hpp"
#include "SGAL/Indexed_face_set.hpp"
#include "SGAL/Epic_kernel.hpp"
#include "SGAL/Epic_polyhedron.hpp"
#include "SGAL/merge_coplanar_facets.hpp"
#include "SGAL/compute_planes.hpp"
#include "SGAL/Field.hpp"

#include "SFG/basic.hpp"
#include "SFG/Snapping_fixture_generator.hpp"
#include "SFG/general_utils.hpp"
#include "SFG/hub_utils.hpp"
#include "SFG/merge_approximate_coplanar_facets.hpp"

SFG_BEGIN_NAMESPACE

//! \brief adds the container to a given scene.
void Snapping_fixture_generator::
add_to_scene(SGAL::Scene_graph* scene_graph) { m_scene_graph = scene_graph; }

//! \brief transforms a 3D container to a 2D container.
void Snapping_fixture_generator::trigger_changed(const SGAL::Field_info*)
{ execute(); }

/* Free memory before exiting the program.
 */
void freeMemAndFinish(std::vector<std::pair<FacetUtil,
                      std::vector<Finger>*>> mapFacetToFingers)
{
  for (auto i = 0; i < mapFacetToFingers.size(); ++i)
    delete mapFacetToFingers[i].second;
}

//! \brief transforms a 3D container to a 2D container.
void Snapping_fixture_generator::execute()
{
#define MAX_ARM 4
#define MIN_ARM 2
  if (! m_workpiece) return;

  if (! m_coplanar_facets_merged) {
    // auto type = m_workpiece->get_polyhedron_type();
    // if (type != SGAL::Indexed_face_set::POLYHEDRON_EPIC)
    m_workpiece->set_polyhedron_type(SGAL::Indexed_face_set::POLYHEDRON_EPIC);
    m_initialized_workpiece =
      boost::get<SGAL::Epic_polyhedron>(m_workpiece->get_polyhedron());
    auto num_facets_before = m_initialized_workpiece.size_of_facets();
    std::cout << "# facets: " << num_facets_before << std::endl;
    if (m_merge_coplanar_facets) {
      typedef SGAL::Epic_kernel                                 Kernel;
      typedef SGAL::Epic_polyhedron                             Polyhedron;
      typedef Kernel::Plane_3                                   Plane;
      typedef boost::is_same<Polyhedron::Plane_3, Plane>        Poly_has_plane;
      Kernel kernel;

      SGAL::compute_planes(kernel, m_initialized_workpiece, Poly_has_plane());
      if (m_approximate)
        SFG::merge_approximate_coplanar_facets(kernel, m_initialized_workpiece,
                                               m_epsilon);
      else SGAL::merge_coplanar_facets(kernel, m_initialized_workpiece,
                                       Poly_has_plane());
      set_workpiece_number_of_facets(m_initialized_workpiece.size_of_facets());

      auto* wnof_field = get_field(WORKPIECE_NUMBER_OF_FACETS);
      if (wnof_field != nullptr) wnof_field->cascade();

      m_coplanar_facets_merged = true;
    }
  }
  auto num_facets = m_initialized_workpiece.size_of_facets();
  std::cout << "# facets: " << num_facets << std::endl;

  // Gets the max size of P for width of hub calculations
  // max_size = get_max_size(poly);

  // Get the mapping of facets (possible palms) to their possible fingers
  std::vector<std::pair<FacetUtil, std::vector<Finger>*>> mapFacetToFingers;
  getPossibleFingersForEveryPalm(m_initialized_workpiece, &mapFacetToFingers,
                                 m_allow_gp_neighbor);

  /* Check for every configuration possible if this configuration forms a valid
   * fixture.
   * If it is valid fixture, 3D model the fixture.
   * The algorithm:
   *   1. Iterate over all possible amount of fingers
   *     a.  Iterate over all possible palms
   *       1) Iterate over all possible subgroup of fingers corresponds the
   *          palm, in the size selected in 1.
   *         a) Check if the configuration obtained forms a valid fixture.
   */
  std::vector<std::vector<int>*> subgroups;
  std::vector<Finger> setOffFingers;
  FacetUtil fingers_base[4];
  FacetUtil fingers_grippers[4];

  bool done(false);

  auto min_f = m_min_number_of_fingers;
  if (min_f < MIN_ARM) min_f = MIN_ARM;
  else if (MAX_ARM < min_f) min_f = MAX_ARM;
  for (auto f = min_f; f <= MAX_ARM; ++f) {
    auto i = (m_min_palm_index < mapFacetToFingers.size()) ? m_min_palm_index : 0;
    for (; i < mapFacetToFingers.size(); ++i) {
      subgroups.clear();
      getSubgroups(f, mapFacetToFingers[i].second->size(), subgroups);

      auto j = (m_min_subset_index < subgroups.size()) ? m_min_subset_index : 0;
      for (; j < subgroups.size(); ++j) {
        setOffFingers.clear();
        for (auto k = 0; k < f; ++k) {
          setOffFingers.push_back(mapFacetToFingers[i].second->at(subgroups[j]->
                                                                  at(k)));
        }

        /* Checking validity of a configuration.
         * The process includes:
         * Checking the fixture forms a covering set.
         * Checking the fixture without the grippers doesn't forms a covering
         * set.
         * Additional user defined checks in aproveSet function.
         */
        if (checkFingersAndBase(mapFacetToFingers[i].first.plane, setOffFingers)
            && aproveSet(setOffFingers))
        {
          // Convert to configuration to facets for the generation process.
          for (auto m = 0; m < f; ++m) {
            Finger ar = setOffFingers[m];
            fingers_base[m] = ar.body;
            fingers_grippers[m] = ar.gripper;
          }

          // Generating a mesh from the hub
          CGAL::Surface_mesh<Kernel::Point_3> fixture_mesh;
          build_hub_from(m_initialized_workpiece, mapFacetToFingers[i].first,
                         fingers_base, fingers_grippers, f,
                         m_fixture_width, m_finger_width,
                         m_gripper_penetration, m_gripper_extra,
                         fixture_mesh);
          Polyhedron fixture;
          CGAL::copy_face_graph(fixture_mesh, fixture);
          // std::cout << fixture << std::endl;
          m_fixture->set_polyhedron(fixture);
          m_fixture->set_triangulate_facets(m_triangulat_facets);
          m_number_of_fingers = f;
          m_palm_index = i;
          m_subset_index = j;
          std::cout << "Success: fingers: " << f
                    << ", palm: " << i << ", subset: " << j << std::endl;
          done = true;
          break;
        }
        if (done) break;
      }
      if (done) break;
    }
    if (done) break;
  }
  if (! done) std::cout << "Failure: cannot find a valid fixture." << std::endl;
  freeMemAndFinish(mapFacetToFingers);
  delSubGroup(subgroups);

  if (done) {
    auto* fixture_field = get_field(FIXTURE);
    if (fixture_field != nullptr) fixture_field->cascade();

    auto* nof_field = get_field(NUMBER_OF_FINGERS);
    if (nof_field != nullptr) nof_field->cascade();

    auto* pi_field = get_field(PALM_INDEX);
    if (pi_field != nullptr) pi_field->cascade();

    auto* si_field = get_field(SUBSET_INDEX);
    if (si_field != nullptr) si_field->cascade();
  }
}

SFG_END_NAMESPACE
