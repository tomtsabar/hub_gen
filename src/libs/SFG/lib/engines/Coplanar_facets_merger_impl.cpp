// Copyright (c) 2004 Israel.
// All rights reserved.
//
// This file is part of SGAL; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation; version 2.1 of the
// License. See the file LICENSE.LGPL distributed with SGAL.
//
// Licensees holding a valid commercial license may use this file in
// accordance with the commercial license agreement provided with the
// software.
//
// This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING
// THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE.
//
// SPDX-License-Identifier: GPL-3.0+
//
// Author(s): Efi Fogel         <efifogel@gmail.com>

#include <boost/type_traits.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Field.hpp"
#include "SGAL/Indexed_face_set.hpp"
#include "SGAL/compute_planes.hpp"
#include "SGAL/merge_coplanar_facets.hpp"
#
#include "SFG/basic.hpp"
#include "SFG/Coplanar_facets_merger.hpp"
#include "SFG/general_utils.hpp"
#include "SFG/merge_approximate_coplanar_facets.hpp"

SFG_BEGIN_NAMESPACE

//! \brief adds the container to a given scene.
void Coplanar_facets_merger::add_to_scene(SGAL::Scene_graph* scene_graph)
{ m_scene_graph = scene_graph; }

//! \brief transforms a 3D container to a 2D container.
void Coplanar_facets_merger::trigger_changed(const SGAL::Field_info*)
{ execute(); }

//! \brief transforms a 3D container to a 2D container.
void Coplanar_facets_merger::execute()
{
  typedef SGAL::Epic_kernel                             Kernel;
  typedef SGAL::Epic_polyhedron                         Polyhedron;
  typedef Kernel::Plane_3                               Plane;
  typedef boost::is_same<Polyhedron::Plane_3, Plane>    Poly_has_plane;
  Kernel kernel;

  m_in->set_polyhedron_type(SGAL::Indexed_face_set::POLYHEDRON_EPIC);
  auto polyhedron = boost::get<SGAL::Epic_polyhedron>(m_in->get_polyhedron());

  auto num_facets = polyhedron.size_of_facets();
  std::cout << "# facets: " << num_facets << std::endl;
  SGAL::compute_planes(kernel, polyhedron, Poly_has_plane());
  if (m_approximate)
    SFG::merge_approximate_coplanar_facets(kernel, polyhedron, m_epsilon);
  else SGAL::merge_coplanar_facets(kernel, polyhedron, Poly_has_plane());
  auto num_coplanar_facets = polyhedron.size_of_facets();
  std::cout << "# facets: " << num_coplanar_facets << std::endl;
  set_number_of_facets(num_facets);

  if (! m_result) {
    m_result = Shared_indexed_face_set(new SGAL::Indexed_face_set);
  }
  m_result->set_polyhedron(polyhedron);

  auto* result_field = get_field(RESULT);
  if (result_field != nullptr) result_field->cascade();

  auto* nof_field = get_field(NUMBER_OF_FACETS);
  if (nof_field != nullptr) nof_field->cascade();
}

SFG_END_NAMESPACE
