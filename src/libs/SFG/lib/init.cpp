// Copyright (c) 2019 Israel.
// All rights reserved to Tel Aviv University.
//
// This file is private property of Tel Aviv University.
//
// Author(s): Efi Fogel         <efifogel@gmail.com>
// Author(s)     : Tom Tsabar         <tomtsabar9@gmail.com>


#if defined(_WIN32)
#pragma warning ( disable : 4146 )
#pragma warning ( disable : 4244 )
#pragma warning ( disable : 4390 )
#pragma warning ( disable : 4503 )
#pragma warning ( disable : 4800 )
#endif

#include <boost/extension/extension.hpp>

#include "SGAL/basic.hpp"
#include "SGAL/Container_factory.hpp"
#include "SGAL/Tracer.hpp"

#include "SFG/basic.hpp"
#include "SFG/Snapping_fixture_generator.hpp"
#include "SFG/Coplanar_facets_merger.hpp"

SFG_BEGIN_NAMESPACE

extern "C" void BOOST_EXTENSION_EXPORT_DECL sfg_init()
{
  // Obtain trace codes:
  // auto* tracer = SGAL::Tracer::get_instance();
  // auto code = tracer->register_option("fixture-positioner");

  // Initialize the containers:
  REGISTER_OBJECT(Snapping_fixture_generator);
  REGISTER_OBJECT(Coplanar_facets_merger);

  // Set the trace codes:
  // Snapping_fixture_generator::set_trace_code(code);
}

SFG_END_NAMESPACE
